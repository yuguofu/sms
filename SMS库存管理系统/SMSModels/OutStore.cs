﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
    public class OutStore
    {
        public string OutStoreID { get; set; }     //出库编号
        public string GoodsID { get; set; }     //货物编号
        public int StoreID { get; set; }    //仓库ID
        public int SupplierID { get; set; }  //供货商ID
        public double OutStoreMonovalent { get; set; }   //出库单价
        public int OutStoreQuantity { get; set; }    //出库数量
        public double OutStoreTotal { get; set; }    //出库总金额
        public string OutStoreTime { get; set; }     //出库时间
        public string OutStoreType { get; set; }    //出库方式
        public string PickUpUnit { get; set; }     //提货单位
        public string Consignee { get; set; }     //提货人
        public string Handler { get; set; }     //经手人
        public string Remarks { get; set; }  //备注
    }
}
