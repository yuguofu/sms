﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
    public class Store
    {
        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public string PIC { get; set; }  //负责人
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Remarks { get; set; }

    }
}
