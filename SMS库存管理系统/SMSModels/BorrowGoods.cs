﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
    public class BorrowGoods
    {
        
        public string BorrowGoodsID { get; set; }  //借货编号
        public string BorrowGoodsUnits { get; set; }     //借货单位
        public string Borrower { get; set; }     //借货人
        public string BorrowerPhone { get; set; }     //电话
        public string GoodsID { get; set; }     //货物编号
        public int StoreID { get; set; }    //仓库ID
        public int BorrowGoodsQuantity { get; set; }     //借货数量
        public string BorrowGoodsTime { get; set; }     //借货时间
        public string Handler { get; set; }     //经手人
        public string Remarks { get; set; }  //备注

        //
        public string GoodsName { get; set; }     //货物名称
        public string StoreName { get; set; }    //仓库名称
        public string Spec { get; set; }  //规格
        public string Units { get; set; }  //计量单位
    }
}
