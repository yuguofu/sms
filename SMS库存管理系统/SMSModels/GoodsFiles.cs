﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
    public class GoodsFiles
    {
        public string GoodsID { get; set; }  //货物编号
        public string GoodsName { get; set; }  //货物名称
        public int SupplierID { get; set; }  //供货商ID,关联供货商实体类的供货商ID
        public string SupplierName { get; set; }  //供货商名称
        public string Spec { get; set; }  //规格
        public string Units { get; set; }  //计量单位
        public double Monovalent { get; set; }  //单价
        public string Remarks { get; set; }  //备注
    }
}
