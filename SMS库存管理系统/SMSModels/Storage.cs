﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
    public class Storage
    {

        public string  GoodsID { get; set; }
        public int StoreID { get; set; }
        public int SupplierID { get; set; }
        public double InStoreMonovalent { get; set; }
        public int StorageNum { get; set; }     //库存数量
        public string Remarks { get; set; }

        //扩展属性
        public string GoodsName { get; set; }
        public string StoreName { get; set; }
        public string SupplierName { get; set; }
        public string Spec { get; set; }
        public string Units { get; set; }
    }
}
