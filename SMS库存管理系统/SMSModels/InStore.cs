﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
   public class InStore
    {
        public string InStoreID { get; set; }     //入库编号
        public string GoodsID { get; set; }     //货物编号
        public int StoreID { get; set; }    //仓库ID
        public int SupplierID { get; set; }  //供货商ID
        public double InStoreMonovalent { get; set; }   //入库单价
        public int InStoreQuantity { get; set; }    //入库数量
        public double InStoreTotal { get; set; }    //入库总金额
        public string InStoreTime { get; set; }     //入库时间
        public string Handler { get; set; }     //经手人
        public string Remarks { get; set; }  //备注
    }
}
