﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSModels
{
    public class ReturnGoods
    {
        public string ReturnGoodsID { get; set; }  //还货编号
        public string ReturnGoodsUnits { get; set; }     //还货单位
        public string Returner { get; set; }     //还货人
        public string ReturnerPhone { get; set; }     //电话
        public string GoodsID { get; set; }     //货物编号
        public string GoodsName { get; set; }     //货物名称
        public string GoodsSpec { get; set; }     //规格
        public string Units { get; set; }     //计量单位
        public int StoreID { get; set; }    //仓库ID
        public string StoreName { get; set; }    //仓库ID
        public int ReturnGoodsQuantity { get; set; }     //还货数量
        public string ReturnGoodsTime { get; set; }     //还货时间
        public string Handler { get; set; }     //经手人
        public string Remarks { get; set; }  //备注
    }
}
