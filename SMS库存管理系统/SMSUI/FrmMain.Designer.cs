﻿namespace SMSUI
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.pnlMainTitle = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblMainTitle = new System.Windows.Forms.Label();
            this.ssr = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusCurrentUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLoginTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusCurrentTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblBaseInfo = new System.Windows.Forms.Label();
            this.lblGoodsManager = new System.Windows.Forms.Label();
            this.lblQueryStatistics = new System.Windows.Forms.Label();
            this.lblParallelLines1 = new System.Windows.Forms.Label();
            this.lblParallelLines2 = new System.Windows.Forms.Label();
            this.lblOther = new System.Windows.Forms.Label();
            this.lblParallelLines3 = new System.Windows.Forms.Label();
            this.tmrCurrentTime = new System.Windows.Forms.Timer(this.components);
            this.picOutStoreYearStatistics = new System.Windows.Forms.PictureBox();
            this.picOutStoreMonthlyStatistics = new System.Windows.Forms.PictureBox();
            this.picReturnGoodsQuery = new System.Windows.Forms.PictureBox();
            this.picBorrowGoodsQuery = new System.Windows.Forms.PictureBox();
            this.picReturnGoods = new System.Windows.Forms.PictureBox();
            this.picStockQuery = new System.Windows.Forms.PictureBox();
            this.picBorrowGoods = new System.Windows.Forms.PictureBox();
            this.picExit = new System.Windows.Forms.PictureBox();
            this.picGoodsInfoSetup = new System.Windows.Forms.PictureBox();
            this.picOutStoreQuery = new System.Windows.Forms.PictureBox();
            this.picOutStoreManager = new System.Windows.Forms.PictureBox();
            this.picInStorageQuery = new System.Windows.Forms.PictureBox();
            this.picAbout = new System.Windows.Forms.PictureBox();
            this.picSupplierSetup = new System.Windows.Forms.PictureBox();
            this.picInStorageManager = new System.Windows.Forms.PictureBox();
            this.picUserManager = new System.Windows.Forms.PictureBox();
            this.picStoreSetup = new System.Windows.Forms.PictureBox();
            this.pnlMainTitle.SuspendLayout();
            this.ssr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreYearStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreMonthlyStatistics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReturnGoodsQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBorrowGoodsQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReturnGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStockQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBorrowGoods)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGoodsInfoSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInStorageQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAbout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSupplierSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInStorageManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUserManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStoreSetup)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMainTitle
            // 
            this.pnlMainTitle.BackColor = System.Drawing.Color.PowderBlue;
            this.pnlMainTitle.Controls.Add(this.label1);
            this.pnlMainTitle.Controls.Add(this.BtnMinimize);
            this.pnlMainTitle.Controls.Add(this.btnClose);
            this.pnlMainTitle.Controls.Add(this.lblMainTitle);
            this.pnlMainTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlMainTitle.Margin = new System.Windows.Forms.Padding(4);
            this.pnlMainTitle.Name = "pnlMainTitle";
            this.pnlMainTitle.Size = new System.Drawing.Size(1067, 100);
            this.pnlMainTitle.TabIndex = 0;
            this.pnlMainTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PnlMainTitle_MouseDown);
            this.pnlMainTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PnlMainTitle_MouseMove);
            this.pnlMainTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PnlMainTitle_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.label1.Location = new System.Drawing.Point(459, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "2.0";
            // 
            // BtnMinimize
            // 
            this.BtnMinimize.AutoSize = true;
            this.BtnMinimize.BackColor = System.Drawing.Color.PowderBlue;
            this.BtnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtnMinimize.FlatAppearance.BorderSize = 0;
            this.BtnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMinimize.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnMinimize.ForeColor = System.Drawing.SystemColors.WindowText;
            this.BtnMinimize.Image = global::SMSUI.Properties.Resources.最小化;
            this.BtnMinimize.Location = new System.Drawing.Point(948, 0);
            this.BtnMinimize.Margin = new System.Windows.Forms.Padding(4);
            this.BtnMinimize.Name = "BtnMinimize";
            this.BtnMinimize.Size = new System.Drawing.Size(61, 39);
            this.BtnMinimize.TabIndex = 1;
            this.BtnMinimize.UseVisualStyleBackColor = false;
            this.BtnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.BackColor = System.Drawing.Color.PowderBlue;
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.WindowText;
            this.btnClose.Image = global::SMSUI.Properties.Resources.关闭;
            this.btnClose.Location = new System.Drawing.Point(1005, 0);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 39);
            this.btnClose.TabIndex = 1;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // lblMainTitle
            // 
            this.lblMainTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblMainTitle.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblMainTitle.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lblMainTitle.Location = new System.Drawing.Point(91, 28);
            this.lblMainTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMainTitle.Name = "lblMainTitle";
            this.lblMainTitle.Size = new System.Drawing.Size(397, 56);
            this.lblMainTitle.TabIndex = 0;
            this.lblMainTitle.Text = "库  存  管  理  系  统";
            // 
            // ssr
            // 
            this.ssr.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ssr.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusCurrentUser,
            this.toolStripStatusLoginTime,
            this.toolStripStatusCurrentTime,
            this.toolStripStatusLabel4});
            this.ssr.Location = new System.Drawing.Point(0, 676);
            this.ssr.Name = "ssr";
            this.ssr.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.ssr.Size = new System.Drawing.Size(1067, 26);
            this.ssr.TabIndex = 1;
            this.ssr.Text = "statusStrip1";
            // 
            // toolStripStatusCurrentUser
            // 
            this.toolStripStatusCurrentUser.Name = "toolStripStatusCurrentUser";
            this.toolStripStatusCurrentUser.Size = new System.Drawing.Size(84, 20);
            this.toolStripStatusCurrentUser.Text = "当前用户：";
            // 
            // toolStripStatusLoginTime
            // 
            this.toolStripStatusLoginTime.Name = "toolStripStatusLoginTime";
            this.toolStripStatusLoginTime.Size = new System.Drawing.Size(84, 20);
            this.toolStripStatusLoginTime.Text = "登录时间：";
            // 
            // toolStripStatusCurrentTime
            // 
            this.toolStripStatusCurrentTime.Name = "toolStripStatusCurrentTime";
            this.toolStripStatusCurrentTime.Size = new System.Drawing.Size(84, 20);
            this.toolStripStatusCurrentTime.Text = "现行时间：";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(103, 20);
            this.toolStripStatusLabel4.Text = "※听雨※  制作";
            // 
            // lblBaseInfo
            // 
            this.lblBaseInfo.BackColor = System.Drawing.Color.LightSkyBlue;
            this.lblBaseInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBaseInfo.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblBaseInfo.Location = new System.Drawing.Point(0, 100);
            this.lblBaseInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBaseInfo.Name = "lblBaseInfo";
            this.lblBaseInfo.Size = new System.Drawing.Size(72, 144);
            this.lblBaseInfo.TabIndex = 2;
            this.lblBaseInfo.Text = "基\r\n本\r\n档\r\n案";
            this.lblBaseInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGoodsManager
            // 
            this.lblGoodsManager.BackColor = System.Drawing.Color.LightSkyBlue;
            this.lblGoodsManager.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblGoodsManager.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblGoodsManager.Location = new System.Drawing.Point(0, 244);
            this.lblGoodsManager.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGoodsManager.Name = "lblGoodsManager";
            this.lblGoodsManager.Size = new System.Drawing.Size(72, 144);
            this.lblGoodsManager.TabIndex = 2;
            this.lblGoodsManager.Text = "货\r\n物\r\n管\r\n理";
            this.lblGoodsManager.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblQueryStatistics
            // 
            this.lblQueryStatistics.BackColor = System.Drawing.Color.LightSkyBlue;
            this.lblQueryStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblQueryStatistics.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblQueryStatistics.Location = new System.Drawing.Point(0, 388);
            this.lblQueryStatistics.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQueryStatistics.Name = "lblQueryStatistics";
            this.lblQueryStatistics.Size = new System.Drawing.Size(72, 144);
            this.lblQueryStatistics.TabIndex = 2;
            this.lblQueryStatistics.Text = "统\r\n计\r\n查\r\n询";
            this.lblQueryStatistics.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblParallelLines1
            // 
            this.lblParallelLines1.BackColor = System.Drawing.Color.Blue;
            this.lblParallelLines1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblParallelLines1.Location = new System.Drawing.Point(72, 244);
            this.lblParallelLines1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParallelLines1.Name = "lblParallelLines1";
            this.lblParallelLines1.Size = new System.Drawing.Size(995, 1);
            this.lblParallelLines1.TabIndex = 3;
            // 
            // lblParallelLines2
            // 
            this.lblParallelLines2.BackColor = System.Drawing.Color.Blue;
            this.lblParallelLines2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblParallelLines2.Location = new System.Drawing.Point(72, 388);
            this.lblParallelLines2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParallelLines2.Name = "lblParallelLines2";
            this.lblParallelLines2.Size = new System.Drawing.Size(995, 1);
            this.lblParallelLines2.TabIndex = 3;
            // 
            // lblOther
            // 
            this.lblOther.BackColor = System.Drawing.Color.LightSkyBlue;
            this.lblOther.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblOther.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblOther.Location = new System.Drawing.Point(0, 531);
            this.lblOther.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOther.Name = "lblOther";
            this.lblOther.Size = new System.Drawing.Size(72, 144);
            this.lblOther.TabIndex = 2;
            this.lblOther.Tag = "";
            this.lblOther.Text = "其\r\n\r\n他";
            this.lblOther.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblParallelLines3
            // 
            this.lblParallelLines3.BackColor = System.Drawing.Color.Blue;
            this.lblParallelLines3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblParallelLines3.Location = new System.Drawing.Point(72, 531);
            this.lblParallelLines3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParallelLines3.Name = "lblParallelLines3";
            this.lblParallelLines3.Size = new System.Drawing.Size(995, 1);
            this.lblParallelLines3.TabIndex = 3;
            // 
            // tmrCurrentTime
            // 
            this.tmrCurrentTime.Interval = 1000;
            this.tmrCurrentTime.Tick += new System.EventHandler(this.tmrCurrentTime_Tick);
            // 
            // picOutStoreYearStatistics
            // 
            this.picOutStoreYearStatistics.BackColor = System.Drawing.Color.Transparent;
            this.picOutStoreYearStatistics.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picOutStoreYearStatistics.Image = global::SMSUI.Properties.Resources.出库货物年统计;
            this.picOutStoreYearStatistics.Location = new System.Drawing.Point(913, 409);
            this.picOutStoreYearStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.picOutStoreYearStatistics.Name = "picOutStoreYearStatistics";
            this.picOutStoreYearStatistics.Size = new System.Drawing.Size(92, 95);
            this.picOutStoreYearStatistics.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutStoreYearStatistics.TabIndex = 4;
            this.picOutStoreYearStatistics.TabStop = false;
            this.picOutStoreYearStatistics.Click += new System.EventHandler(this.PicOutStoreYearStatistics_Click);
            // 
            // picOutStoreMonthlyStatistics
            // 
            this.picOutStoreMonthlyStatistics.BackColor = System.Drawing.Color.Transparent;
            this.picOutStoreMonthlyStatistics.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picOutStoreMonthlyStatistics.Image = global::SMSUI.Properties.Resources.出库货物月统计;
            this.picOutStoreMonthlyStatistics.Location = new System.Drawing.Point(776, 409);
            this.picOutStoreMonthlyStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.picOutStoreMonthlyStatistics.Name = "picOutStoreMonthlyStatistics";
            this.picOutStoreMonthlyStatistics.Size = new System.Drawing.Size(92, 95);
            this.picOutStoreMonthlyStatistics.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutStoreMonthlyStatistics.TabIndex = 4;
            this.picOutStoreMonthlyStatistics.TabStop = false;
            this.picOutStoreMonthlyStatistics.Click += new System.EventHandler(this.PicOutStoreMonthlyStatistics_Click);
            // 
            // picReturnGoodsQuery
            // 
            this.picReturnGoodsQuery.BackColor = System.Drawing.Color.Transparent;
            this.picReturnGoodsQuery.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picReturnGoodsQuery.Image = global::SMSUI.Properties.Resources.货物归还查询;
            this.picReturnGoodsQuery.Location = new System.Drawing.Point(641, 409);
            this.picReturnGoodsQuery.Margin = new System.Windows.Forms.Padding(4);
            this.picReturnGoodsQuery.Name = "picReturnGoodsQuery";
            this.picReturnGoodsQuery.Size = new System.Drawing.Size(92, 95);
            this.picReturnGoodsQuery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picReturnGoodsQuery.TabIndex = 4;
            this.picReturnGoodsQuery.TabStop = false;
            this.picReturnGoodsQuery.Click += new System.EventHandler(this.PicReturnGoodsQuery_Click);
            // 
            // picBorrowGoodsQuery
            // 
            this.picBorrowGoodsQuery.BackColor = System.Drawing.Color.Transparent;
            this.picBorrowGoodsQuery.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBorrowGoodsQuery.Image = global::SMSUI.Properties.Resources.货物借出查询;
            this.picBorrowGoodsQuery.Location = new System.Drawing.Point(507, 409);
            this.picBorrowGoodsQuery.Margin = new System.Windows.Forms.Padding(4);
            this.picBorrowGoodsQuery.Name = "picBorrowGoodsQuery";
            this.picBorrowGoodsQuery.Size = new System.Drawing.Size(92, 95);
            this.picBorrowGoodsQuery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBorrowGoodsQuery.TabIndex = 4;
            this.picBorrowGoodsQuery.TabStop = false;
            this.picBorrowGoodsQuery.Click += new System.EventHandler(this.PicBorrowGoodsQuery_Click);
            // 
            // picReturnGoods
            // 
            this.picReturnGoods.BackColor = System.Drawing.Color.Transparent;
            this.picReturnGoods.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picReturnGoods.Image = global::SMSUI.Properties.Resources.还货;
            this.picReturnGoods.Location = new System.Drawing.Point(841, 268);
            this.picReturnGoods.Margin = new System.Windows.Forms.Padding(4);
            this.picReturnGoods.Name = "picReturnGoods";
            this.picReturnGoods.Size = new System.Drawing.Size(92, 95);
            this.picReturnGoods.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picReturnGoods.TabIndex = 4;
            this.picReturnGoods.TabStop = false;
            this.picReturnGoods.Click += new System.EventHandler(this.picReturnGoods_Click);
            // 
            // picStockQuery
            // 
            this.picStockQuery.BackColor = System.Drawing.Color.Transparent;
            this.picStockQuery.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picStockQuery.Image = global::SMSUI.Properties.Resources.库存查询;
            this.picStockQuery.Location = new System.Drawing.Point(372, 409);
            this.picStockQuery.Margin = new System.Windows.Forms.Padding(4);
            this.picStockQuery.Name = "picStockQuery";
            this.picStockQuery.Size = new System.Drawing.Size(92, 95);
            this.picStockQuery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStockQuery.TabIndex = 4;
            this.picStockQuery.TabStop = false;
            this.picStockQuery.Click += new System.EventHandler(this.PicStockQuery_Click);
            // 
            // picBorrowGoods
            // 
            this.picBorrowGoods.BackColor = System.Drawing.Color.Transparent;
            this.picBorrowGoods.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBorrowGoods.Image = global::SMSUI.Properties.Resources.借货;
            this.picBorrowGoods.Location = new System.Drawing.Point(621, 268);
            this.picBorrowGoods.Margin = new System.Windows.Forms.Padding(4);
            this.picBorrowGoods.Name = "picBorrowGoods";
            this.picBorrowGoods.Size = new System.Drawing.Size(92, 95);
            this.picBorrowGoods.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBorrowGoods.TabIndex = 4;
            this.picBorrowGoods.TabStop = false;
            this.picBorrowGoods.Click += new System.EventHandler(this.picBorrowGoods_Click);
            // 
            // picExit
            // 
            this.picExit.BackColor = System.Drawing.Color.Transparent;
            this.picExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picExit.Image = global::SMSUI.Properties.Resources.退出系统;
            this.picExit.Location = new System.Drawing.Point(699, 558);
            this.picExit.Margin = new System.Windows.Forms.Padding(4);
            this.picExit.Name = "picExit";
            this.picExit.Size = new System.Drawing.Size(92, 95);
            this.picExit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picExit.TabIndex = 4;
            this.picExit.TabStop = false;
            this.picExit.Click += new System.EventHandler(this.PicExit_Click);
            // 
            // picGoodsInfoSetup
            // 
            this.picGoodsInfoSetup.BackColor = System.Drawing.Color.Transparent;
            this.picGoodsInfoSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picGoodsInfoSetup.Image = global::SMSUI.Properties.Resources.货物档案设置;
            this.picGoodsInfoSetup.Location = new System.Drawing.Point(699, 124);
            this.picGoodsInfoSetup.Margin = new System.Windows.Forms.Padding(4);
            this.picGoodsInfoSetup.Name = "picGoodsInfoSetup";
            this.picGoodsInfoSetup.Size = new System.Drawing.Size(92, 95);
            this.picGoodsInfoSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picGoodsInfoSetup.TabIndex = 4;
            this.picGoodsInfoSetup.TabStop = false;
            this.picGoodsInfoSetup.Click += new System.EventHandler(this.picGoodsInfoSetup_Click);
            // 
            // picOutStoreQuery
            // 
            this.picOutStoreQuery.BackColor = System.Drawing.Color.Transparent;
            this.picOutStoreQuery.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picOutStoreQuery.Image = global::SMSUI.Properties.Resources.出库查询;
            this.picOutStoreQuery.Location = new System.Drawing.Point(233, 409);
            this.picOutStoreQuery.Margin = new System.Windows.Forms.Padding(4);
            this.picOutStoreQuery.Name = "picOutStoreQuery";
            this.picOutStoreQuery.Size = new System.Drawing.Size(92, 95);
            this.picOutStoreQuery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutStoreQuery.TabIndex = 4;
            this.picOutStoreQuery.TabStop = false;
            this.picOutStoreQuery.Click += new System.EventHandler(this.PicOutStoreQuery_Click);
            // 
            // picOutStoreManager
            // 
            this.picOutStoreManager.BackColor = System.Drawing.Color.Transparent;
            this.picOutStoreManager.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picOutStoreManager.Image = global::SMSUI.Properties.Resources.出库管理;
            this.picOutStoreManager.Location = new System.Drawing.Point(397, 268);
            this.picOutStoreManager.Margin = new System.Windows.Forms.Padding(4);
            this.picOutStoreManager.Name = "picOutStoreManager";
            this.picOutStoreManager.Size = new System.Drawing.Size(92, 95);
            this.picOutStoreManager.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutStoreManager.TabIndex = 4;
            this.picOutStoreManager.TabStop = false;
            this.picOutStoreManager.Click += new System.EventHandler(this.picOutStoreManager_Click);
            // 
            // picInStorageQuery
            // 
            this.picInStorageQuery.BackColor = System.Drawing.Color.Transparent;
            this.picInStorageQuery.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picInStorageQuery.Image = global::SMSUI.Properties.Resources.入库查询;
            this.picInStorageQuery.Location = new System.Drawing.Point(97, 409);
            this.picInStorageQuery.Margin = new System.Windows.Forms.Padding(4);
            this.picInStorageQuery.Name = "picInStorageQuery";
            this.picInStorageQuery.Size = new System.Drawing.Size(92, 95);
            this.picInStorageQuery.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picInStorageQuery.TabIndex = 4;
            this.picInStorageQuery.TabStop = false;
            this.picInStorageQuery.Click += new System.EventHandler(this.picInStorageQuery_Click);
            // 
            // picAbout
            // 
            this.picAbout.BackColor = System.Drawing.Color.Transparent;
            this.picAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picAbout.Image = global::SMSUI.Properties.Resources.关于本系统;
            this.picAbout.Location = new System.Drawing.Point(469, 558);
            this.picAbout.Margin = new System.Windows.Forms.Padding(4);
            this.picAbout.Name = "picAbout";
            this.picAbout.Size = new System.Drawing.Size(92, 95);
            this.picAbout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAbout.TabIndex = 4;
            this.picAbout.TabStop = false;
            this.picAbout.Click += new System.EventHandler(this.picAbout_Click);
            // 
            // picSupplierSetup
            // 
            this.picSupplierSetup.BackColor = System.Drawing.Color.Transparent;
            this.picSupplierSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picSupplierSetup.Image = global::SMSUI.Properties.Resources.供应商设置;
            this.picSupplierSetup.Location = new System.Drawing.Point(469, 124);
            this.picSupplierSetup.Margin = new System.Windows.Forms.Padding(4);
            this.picSupplierSetup.Name = "picSupplierSetup";
            this.picSupplierSetup.Size = new System.Drawing.Size(92, 95);
            this.picSupplierSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSupplierSetup.TabIndex = 4;
            this.picSupplierSetup.TabStop = false;
            this.picSupplierSetup.Click += new System.EventHandler(this.picSupplierSetup_Click);
            // 
            // picInStorageManager
            // 
            this.picInStorageManager.BackColor = System.Drawing.Color.Transparent;
            this.picInStorageManager.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picInStorageManager.Image = global::SMSUI.Properties.Resources.入库管理;
            this.picInStorageManager.Location = new System.Drawing.Point(189, 268);
            this.picInStorageManager.Margin = new System.Windows.Forms.Padding(4);
            this.picInStorageManager.Name = "picInStorageManager";
            this.picInStorageManager.Size = new System.Drawing.Size(92, 95);
            this.picInStorageManager.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picInStorageManager.TabIndex = 4;
            this.picInStorageManager.TabStop = false;
            this.picInStorageManager.Click += new System.EventHandler(this.picInStorageManager_Click);
            // 
            // picUserManager
            // 
            this.picUserManager.BackColor = System.Drawing.Color.Transparent;
            this.picUserManager.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picUserManager.Image = global::SMSUI.Properties.Resources.用户管理;
            this.picUserManager.Location = new System.Drawing.Point(233, 558);
            this.picUserManager.Margin = new System.Windows.Forms.Padding(4);
            this.picUserManager.Name = "picUserManager";
            this.picUserManager.Size = new System.Drawing.Size(92, 95);
            this.picUserManager.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picUserManager.TabIndex = 4;
            this.picUserManager.TabStop = false;
            this.picUserManager.Click += new System.EventHandler(this.PicUserManager_Click);
            // 
            // picStoreSetup
            // 
            this.picStoreSetup.BackColor = System.Drawing.Color.Transparent;
            this.picStoreSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picStoreSetup.Image = global::SMSUI.Properties.Resources.仓库设置;
            this.picStoreSetup.Location = new System.Drawing.Point(233, 124);
            this.picStoreSetup.Margin = new System.Windows.Forms.Padding(4);
            this.picStoreSetup.Name = "picStoreSetup";
            this.picStoreSetup.Size = new System.Drawing.Size(92, 95);
            this.picStoreSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picStoreSetup.TabIndex = 4;
            this.picStoreSetup.TabStop = false;
            this.picStoreSetup.Click += new System.EventHandler(this.picStoreSetup_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(1067, 702);
            this.Controls.Add(this.picOutStoreYearStatistics);
            this.Controls.Add(this.picOutStoreMonthlyStatistics);
            this.Controls.Add(this.picReturnGoodsQuery);
            this.Controls.Add(this.picBorrowGoodsQuery);
            this.Controls.Add(this.picReturnGoods);
            this.Controls.Add(this.picStockQuery);
            this.Controls.Add(this.picBorrowGoods);
            this.Controls.Add(this.picExit);
            this.Controls.Add(this.picGoodsInfoSetup);
            this.Controls.Add(this.picOutStoreQuery);
            this.Controls.Add(this.picOutStoreManager);
            this.Controls.Add(this.picInStorageQuery);
            this.Controls.Add(this.picAbout);
            this.Controls.Add(this.picSupplierSetup);
            this.Controls.Add(this.picInStorageManager);
            this.Controls.Add(this.picUserManager);
            this.Controls.Add(this.picStoreSetup);
            this.Controls.Add(this.lblParallelLines3);
            this.Controls.Add(this.lblParallelLines2);
            this.Controls.Add(this.lblParallelLines1);
            this.Controls.Add(this.lblOther);
            this.Controls.Add(this.lblQueryStatistics);
            this.Controls.Add(this.lblGoodsManager);
            this.Controls.Add(this.lblBaseInfo);
            this.Controls.Add(this.ssr);
            this.Controls.Add(this.pnlMainTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "库存管理系统主窗口";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FrmMain_Paint);
            this.pnlMainTitle.ResumeLayout(false);
            this.pnlMainTitle.PerformLayout();
            this.ssr.ResumeLayout(false);
            this.ssr.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreYearStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreMonthlyStatistics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReturnGoodsQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBorrowGoodsQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picReturnGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStockQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBorrowGoods)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGoodsInfoSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picOutStoreManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInStorageQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAbout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSupplierSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInStorageManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUserManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStoreSetup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainTitle;
        private System.Windows.Forms.Label lblMainTitle;
        private System.Windows.Forms.StatusStrip ssr;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusCurrentUser;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLoginTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusCurrentTime;
        private System.Windows.Forms.Label lblBaseInfo;
        private System.Windows.Forms.Label lblGoodsManager;
        private System.Windows.Forms.Label lblQueryStatistics;
        private System.Windows.Forms.Label lblParallelLines1;
        private System.Windows.Forms.Label lblParallelLines2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox picStoreSetup;
        private System.Windows.Forms.PictureBox picSupplierSetup;
        private System.Windows.Forms.PictureBox picGoodsInfoSetup;
        private System.Windows.Forms.PictureBox picInStorageManager;
        private System.Windows.Forms.PictureBox picOutStoreManager;
        private System.Windows.Forms.PictureBox picBorrowGoods;
        private System.Windows.Forms.PictureBox picReturnGoods;
        private System.Windows.Forms.PictureBox picInStorageQuery;
        private System.Windows.Forms.PictureBox picOutStoreQuery;
        private System.Windows.Forms.PictureBox picStockQuery;
        private System.Windows.Forms.PictureBox picBorrowGoodsQuery;
        private System.Windows.Forms.PictureBox picReturnGoodsQuery;
        private System.Windows.Forms.PictureBox picOutStoreMonthlyStatistics;
        private System.Windows.Forms.PictureBox picOutStoreYearStatistics;
        private System.Windows.Forms.Label lblOther;
        private System.Windows.Forms.Label lblParallelLines3;
        private System.Windows.Forms.PictureBox picUserManager;
        private System.Windows.Forms.PictureBox picAbout;
        private System.Windows.Forms.PictureBox picExit;
        private System.Windows.Forms.Button BtnMinimize;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.Timer tmrCurrentTime;
        private System.Windows.Forms.Label label1;
    }
}

