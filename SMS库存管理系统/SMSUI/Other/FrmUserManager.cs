﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using SMSModels;


namespace SMSUI.Other
{
    public partial class FrmUserManager : Form
    {
        BLL.BaseIndo.UserManager userManager = new BLL.BaseIndo.UserManager();

        public FrmUserManager()
        {
            InitializeComponent();

            
        }
        

        private void FrmUserManager_Load(object sender, EventArgs e)
        {
            DataSet ds = userManager.GetDataSet();
            dgvUserInfo.DataSource = ds.Tables["userInfo"];
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DgvUserInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtUserID.Text= Convert.ToString(dgvUserInfo[0, dgvUserInfo.CurrentCell.RowIndex].Value);
            txtUserName.Text = Convert.ToString(dgvUserInfo[1, dgvUserInfo.CurrentCell.RowIndex].Value);
            txtPassword.Text = Convert.ToString(dgvUserInfo[2, dgvUserInfo.CurrentCell.RowIndex].Value);
            cmbPower.Text= Convert.ToString(dgvUserInfo[3, dgvUserInfo.CurrentCell.RowIndex].Value);
            cmbUserType.Text= Convert.ToString(dgvUserInfo[4, dgvUserInfo.CurrentCell.RowIndex].Value);
            txtRemarks.Text= Convert.ToString(dgvUserInfo[5, dgvUserInfo.CurrentCell.RowIndex].Value);
        }

        //添加用户
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Length==0 || txtPassword.Text.Length==0 || cmbPower.Text.Length==0 || cmbUserType.Text.Length==0)
            {
                MessageBox.Show("用户名、密码、用户类型、权限不能为空！");
            }
            else
            {
                User user = new User
                {
                    UserName = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    Power = Convert.ToInt32(cmbPower.Text),
                    UserType = cmbUserType.Text,
                    Remarks = txtRemarks.Text.Trim()
                };

                if (userManager.AddUser(user) > 0)
                {
                    MessageBox.Show("用户添加成功！");
                    FrmUserManager_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("用户添加失败！请检查！");
                }
            }
        }

        //修改用户信息
        private void BtnRevise_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Length == 0 || txtPassword.Text.Length == 0 || cmbPower.Text.Length == 0 || cmbUserType.Text.Length == 0)
            {
                MessageBox.Show("用户名、密码、用户类型、权限不能为空！");
            }
            else
            {
                User user = new User
                {
                    UserID = Convert.ToInt32(txtUserID.Text),
                    UserName = txtUserName.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    Power = Convert.ToInt32(cmbPower.Text),
                    UserType = cmbUserType.Text,
                    Remarks = txtRemarks.Text.Trim()
                };

                if (userManager.UpdateUserInfo(user) > 0)
                {
                    MessageBox.Show("用户信息修改成功！");
                    FrmUserManager_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("用户信息修改失败！请检查！");
                }
            }
        }

        //删除用户
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (dgvUserInfo.CurrentCell != null && dgvUserInfo.CurrentCell.RowIndex != -1)
            {
                User user = new User {
                    UserID = Convert.ToInt32(dgvUserInfo[0, dgvUserInfo.CurrentCell.RowIndex].Value)
                
                };

                if (userManager.DeleteUser(user) > 0)
                {
                    MessageBox.Show("用户删除成功！");
                    FrmUserManager_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("用户删除失败！");
                }
            }
            else
            {
                MessageBox.Show("请在列表中选择要删除的用户！", "提示");
            }
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtUserName,"用户名唯一，不可重复");
        }
    }
}
