﻿namespace SMSUI.BaseInfo
{
    partial class FrmGoodsFilesSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpOperate = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRevise = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbSupplierName = new System.Windows.Forms.ComboBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMonovalent = new System.Windows.Forms.TextBox();
            this.lblMonovalent = new System.Windows.Forms.Label();
            this.txtUnits = new System.Windows.Forms.TextBox();
            this.lblUnits = new System.Windows.Forms.Label();
            this.txtSpec = new System.Windows.Forms.TextBox();
            this.lblSpe = new System.Windows.Forms.Label();
            this.txtGoodsName = new System.Windows.Forms.TextBox();
            this.lblGoodsName = new System.Windows.Forms.Label();
            this.txtGoodsID = new System.Windows.Forms.TextBox();
            this.lblGoodsId = new System.Windows.Forms.Label();
            this.lblSupplierName = new System.Windows.Forms.Label();
            this.dgvGoodsFilesInfo = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.grpOperate.SuspendLayout();
            this.grpInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoodsFilesInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // grpOperate
            // 
            this.grpOperate.Controls.Add(this.btnExit);
            this.grpOperate.Controls.Add(this.btnDelete);
            this.grpOperate.Controls.Add(this.btnRevise);
            this.grpOperate.Controls.Add(this.btnAdd);
            this.grpOperate.Location = new System.Drawing.Point(867, 15);
            this.grpOperate.Margin = new System.Windows.Forms.Padding(4);
            this.grpOperate.Name = "grpOperate";
            this.grpOperate.Padding = new System.Windows.Forms.Padding(4);
            this.grpOperate.Size = new System.Drawing.Size(181, 271);
            this.grpOperate.TabIndex = 6;
            this.grpOperate.TabStop = false;
            this.grpOperate.Text = "操作";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(43, 214);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 29);
            this.btnExit.TabIndex = 11;
            this.btnExit.Text = "退出";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(43, 146);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 10;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnRevise
            // 
            this.btnRevise.Location = new System.Drawing.Point(43, 85);
            this.btnRevise.Margin = new System.Windows.Forms.Padding(4);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(100, 29);
            this.btnRevise.TabIndex = 9;
            this.btnRevise.Text = "修改";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(43, 25);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 29);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "添加";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // grpInfo
            // 
            this.grpInfo.Controls.Add(this.label4);
            this.grpInfo.Controls.Add(this.cmbSupplierName);
            this.grpInfo.Controls.Add(this.txtRemarks);
            this.grpInfo.Controls.Add(this.label3);
            this.grpInfo.Controls.Add(this.txtMonovalent);
            this.grpInfo.Controls.Add(this.lblMonovalent);
            this.grpInfo.Controls.Add(this.txtUnits);
            this.grpInfo.Controls.Add(this.lblUnits);
            this.grpInfo.Controls.Add(this.txtSpec);
            this.grpInfo.Controls.Add(this.lblSpe);
            this.grpInfo.Controls.Add(this.txtGoodsName);
            this.grpInfo.Controls.Add(this.lblGoodsName);
            this.grpInfo.Controls.Add(this.txtGoodsID);
            this.grpInfo.Controls.Add(this.lblGoodsId);
            this.grpInfo.Controls.Add(this.lblSupplierName);
            this.grpInfo.Location = new System.Drawing.Point(16, 15);
            this.grpInfo.Margin = new System.Windows.Forms.Padding(4);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Padding = new System.Windows.Forms.Padding(4);
            this.grpInfo.Size = new System.Drawing.Size(819, 271);
            this.grpInfo.TabIndex = 5;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "信息";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label4.Location = new System.Drawing.Point(52, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(595, 14);
            this.label4.TabIndex = 11;
            this.label4.Text = "注意：货物编号唯一，货物名称、供货商、规格、计量单位、单价等只要有不同使用不同编号。";
            // 
            // cmbSupplierName
            // 
            this.cmbSupplierName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSupplierName.FormattingEnabled = true;
            this.cmbSupplierName.Location = new System.Drawing.Point(152, 86);
            this.cmbSupplierName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbSupplierName.Name = "cmbSupplierName";
            this.cmbSupplierName.Size = new System.Drawing.Size(200, 23);
            this.cmbSupplierName.TabIndex = 3;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(152, 206);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(4);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(592, 25);
            this.txtRemarks.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 212);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "备    注：";
            // 
            // txtMonovalent
            // 
            this.txtMonovalent.Location = new System.Drawing.Point(544, 146);
            this.txtMonovalent.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonovalent.Name = "txtMonovalent";
            this.txtMonovalent.Size = new System.Drawing.Size(200, 25);
            this.txtMonovalent.TabIndex = 6;
            // 
            // lblMonovalent
            // 
            this.lblMonovalent.AutoSize = true;
            this.lblMonovalent.Location = new System.Drawing.Point(444, 152);
            this.lblMonovalent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonovalent.Name = "lblMonovalent";
            this.lblMonovalent.Size = new System.Drawing.Size(84, 15);
            this.lblMonovalent.TabIndex = 10;
            this.lblMonovalent.Text = "单    价：";
            // 
            // txtUnits
            // 
            this.txtUnits.Location = new System.Drawing.Point(152, 148);
            this.txtUnits.Margin = new System.Windows.Forms.Padding(4);
            this.txtUnits.Name = "txtUnits";
            this.txtUnits.Size = new System.Drawing.Size(200, 25);
            this.txtUnits.TabIndex = 5;
            // 
            // lblUnits
            // 
            this.lblUnits.AutoSize = true;
            this.lblUnits.Location = new System.Drawing.Point(52, 154);
            this.lblUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnits.Name = "lblUnits";
            this.lblUnits.Size = new System.Drawing.Size(82, 15);
            this.lblUnits.TabIndex = 10;
            this.lblUnits.Text = "计量单位：";
            // 
            // txtSpec
            // 
            this.txtSpec.Location = new System.Drawing.Point(544, 86);
            this.txtSpec.Margin = new System.Windows.Forms.Padding(4);
            this.txtSpec.Name = "txtSpec";
            this.txtSpec.Size = new System.Drawing.Size(200, 25);
            this.txtSpec.TabIndex = 4;
            // 
            // lblSpe
            // 
            this.lblSpe.AutoSize = true;
            this.lblSpe.Location = new System.Drawing.Point(444, 92);
            this.lblSpe.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSpe.Name = "lblSpe";
            this.lblSpe.Size = new System.Drawing.Size(84, 15);
            this.lblSpe.TabIndex = 10;
            this.lblSpe.Text = "规    格：";
            // 
            // txtGoodsName
            // 
            this.txtGoodsName.Location = new System.Drawing.Point(544, 25);
            this.txtGoodsName.Margin = new System.Windows.Forms.Padding(4);
            this.txtGoodsName.Name = "txtGoodsName";
            this.txtGoodsName.Size = new System.Drawing.Size(200, 25);
            this.txtGoodsName.TabIndex = 2;
            // 
            // lblGoodsName
            // 
            this.lblGoodsName.AutoSize = true;
            this.lblGoodsName.Location = new System.Drawing.Point(446, 32);
            this.lblGoodsName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGoodsName.Name = "lblGoodsName";
            this.lblGoodsName.Size = new System.Drawing.Size(82, 15);
            this.lblGoodsName.TabIndex = 10;
            this.lblGoodsName.Text = "货物名称：";
            // 
            // txtGoodsID
            // 
            this.txtGoodsID.Location = new System.Drawing.Point(152, 25);
            this.txtGoodsID.Margin = new System.Windows.Forms.Padding(4);
            this.txtGoodsID.Name = "txtGoodsID";
            this.txtGoodsID.Size = new System.Drawing.Size(200, 25);
            this.txtGoodsID.TabIndex = 1;
            this.txtGoodsID.Enter += new System.EventHandler(this.txtGoodsId_Enter);
            // 
            // lblGoodsId
            // 
            this.lblGoodsId.AutoSize = true;
            this.lblGoodsId.Location = new System.Drawing.Point(52, 32);
            this.lblGoodsId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGoodsId.Name = "lblGoodsId";
            this.lblGoodsId.Size = new System.Drawing.Size(82, 15);
            this.lblGoodsId.TabIndex = 10;
            this.lblGoodsId.Text = "货物编号：";
            // 
            // lblSupplierName
            // 
            this.lblSupplierName.AutoSize = true;
            this.lblSupplierName.Location = new System.Drawing.Point(52, 92);
            this.lblSupplierName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSupplierName.Name = "lblSupplierName";
            this.lblSupplierName.Size = new System.Drawing.Size(97, 15);
            this.lblSupplierName.TabIndex = 10;
            this.lblSupplierName.Text = "供货商名称：";
            // 
            // dgvGoodsFilesInfo
            // 
            this.dgvGoodsFilesInfo.AllowUserToAddRows = false;
            this.dgvGoodsFilesInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvGoodsFilesInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGoodsFilesInfo.Location = new System.Drawing.Point(16, 318);
            this.dgvGoodsFilesInfo.Margin = new System.Windows.Forms.Padding(4);
            this.dgvGoodsFilesInfo.MultiSelect = false;
            this.dgvGoodsFilesInfo.Name = "dgvGoodsFilesInfo";
            this.dgvGoodsFilesInfo.ReadOnly = true;
            this.dgvGoodsFilesInfo.RowHeadersWidth = 51;
            this.dgvGoodsFilesInfo.RowTemplate.Height = 23;
            this.dgvGoodsFilesInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvGoodsFilesInfo.Size = new System.Drawing.Size(1033, 252);
            this.dgvGoodsFilesInfo.TabIndex = 12;
            this.dgvGoodsFilesInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSupplierInfo_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 299);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "计量单位：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 299);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "档案记录：";
            // 
            // FrmGoodsFilesSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 582);
            this.Controls.Add(this.dgvGoodsFilesInfo);
            this.Controls.Add(this.grpOperate);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmGoodsFilesSetup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "货物档案设置";
            this.Load += new System.EventHandler(this.FrmGoodsFilesSetup_Load);
            this.grpOperate.ResumeLayout(false);
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGoodsFilesInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpOperate;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.TextBox txtMonovalent;
        private System.Windows.Forms.Label lblMonovalent;
        private System.Windows.Forms.TextBox txtUnits;
        private System.Windows.Forms.Label lblUnits;
        private System.Windows.Forms.TextBox txtSpec;
        private System.Windows.Forms.Label lblSpe;
        private System.Windows.Forms.TextBox txtGoodsName;
        private System.Windows.Forms.Label lblGoodsName;
        private System.Windows.Forms.TextBox txtGoodsID;
        private System.Windows.Forms.Label lblGoodsId;
        private System.Windows.Forms.Label lblSupplierName;
        private System.Windows.Forms.DataGridView dgvGoodsFilesInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbSupplierName;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}