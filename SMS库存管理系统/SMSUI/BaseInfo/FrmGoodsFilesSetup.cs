﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using SMSModels;

namespace SMSUI.BaseInfo
{
    public partial class FrmGoodsFilesSetup : Form
    {
        BLL.BaseIndo.GoodsFilesManager goodsFilesManager = new BLL.BaseIndo.GoodsFilesManager();
        List<GoodsFiles> list;
        public FrmGoodsFilesSetup()
        {
            InitializeComponent();
        }
        //窗体加载
        private void FrmGoodsFilesSetup_Load(object sender, EventArgs e)
        {
            DataSet myds = goodsFilesManager.GetDataSet();
            dgvGoodsFilesInfo.DataSource = myds.Tables["goodsFilesInfo"];

            //
            list = goodsFilesManager.GetGoodsInfo();
            cmbSupplierName.DataSource = list;
            cmbSupplierName.DisplayMember = "SupplierName";
            cmbSupplierName.ValueMember = "SupplierID"; 
            cmbSupplierName.SelectedIndex = -1;
            
        }
        //退出按钮单击
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //列表单击
        private void dgvSupplierInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtGoodsID.Text = dgvGoodsFilesInfo[0,dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
            txtGoodsName.Text = dgvGoodsFilesInfo[1,dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
            
            //cmbSupplierName.Text= dgvGoodsFilesInfo[2, dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
            cmbSupplierName.SelectedIndex = -1;
            
            txtSpec.Text = dgvGoodsFilesInfo[3, dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
            txtUnits.Text = dgvGoodsFilesInfo[4, dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
            txtMonovalent.Text = dgvGoodsFilesInfo[5, dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
            txtRemarks.Text= dgvGoodsFilesInfo[6, dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString();
        }
        //显示提示
        private void txtGoodsId_Enter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtGoodsID, "货物编号唯一，货物名称、供货商、规格、计量单位、单价等只要有不同使用不同编号");
        }

        //添加货物档案
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtGoodsID.Text.Trim().Length == 0 || txtGoodsName.Text.Trim().Length == 0 || cmbSupplierName.Text.Trim().Length == 0 || txtSpec.Text.Trim().Length == 0 || txtUnits.Text.Trim().Length == 0 || txtMonovalent.Text.Trim().Length == 0)
            {
                MessageBox.Show("货物编号、货物名称、供货商名称、规格、计量单位、单价不能为空！");
            }
            else
            {
                GoodsFiles goodsFiles=new GoodsFiles
                {
                    GoodsID = txtGoodsID.Text.Trim(),
                    GoodsName = txtGoodsName.Text.Trim(),
                    SupplierID = Convert.ToInt32(cmbSupplierName.SelectedValue),
                    Spec = txtSpec.Text.Trim(),
                    Units = txtUnits.Text.Trim(),
                    Monovalent=Convert.ToDouble(txtMonovalent.Text.Trim()),
                    Remarks = txtRemarks.Text.Trim()
                };

                if (goodsFilesManager.GoodsIDIsExist(txtGoodsID.Text))  //编号存在
                {
                    MessageBox.Show("该货物编号已存在！"); 
                }
                else   //编号不存在
                {
                    if (goodsFilesManager.AddGoodsFile(goodsFiles) > 0)
                    {
                        MessageBox.Show("货物档案添加成功！");
                        FrmGoodsFilesSetup_Load(sender, e);
                    }
                    else
                    {
                        MessageBox.Show("货物档案添加失败！请检查！");
                    }
                }
            }
        }


        //修改货物档案信息
        private void btnRevise_Click(object sender, EventArgs e)
        {
            if (txtGoodsID.Text.Trim().Length == 0 || txtGoodsName.Text.Trim().Length == 0 || cmbSupplierName.Text.Trim().Length == 0 || txtSpec.Text.Trim().Length == 0 || txtUnits.Text.Trim().Length == 0 || txtMonovalent.Text.Trim().Length == 0)
            {
                MessageBox.Show("货物编号、货物名称、供货商名称、规格、计量单位、单价不能为空！");
            }
            else
            {
                GoodsFiles goodsFiles = new GoodsFiles
                {
                    
                    GoodsID = txtGoodsID.Text.Trim(),
                    GoodsName = txtGoodsName.Text.Trim(),
                    SupplierID = Convert.ToInt32(cmbSupplierName.SelectedValue),
                    Spec = txtSpec.Text.Trim(),
                    Units = txtUnits.Text.Trim(),
                    Monovalent = Convert.ToDouble(txtMonovalent.Text.Trim()),
                    Remarks = txtRemarks.Text.Trim()
                };

                if (goodsFilesManager.UpdateGoodsFilesInfo(goodsFiles) > 0)
                {
                    MessageBox.Show("修改货物档案信息成功！");
                    FrmGoodsFilesSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("修改货物档案信息失败！请检查！");
                }
            }
        }

        //删除货物档案
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvGoodsFilesInfo.CurrentCell != null && dgvGoodsFilesInfo.CurrentCell.RowIndex != -1)
            {
                GoodsFiles goodsFiles=new GoodsFiles
                {
                    GoodsID = dgvGoodsFilesInfo[0, dgvGoodsFilesInfo.CurrentCell.RowIndex].Value.ToString()

                };

                if (goodsFilesManager.DeleteGoodsFiles(goodsFiles) > 0)
                {
                    MessageBox.Show("删除成功！");
                    FrmGoodsFilesSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("删除失败！");
                }
            }
            else
            {
                MessageBox.Show("请在列表中选择要删除的货物档案！", "提示");
            }
        }


        

    }
}
