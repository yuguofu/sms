﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using SMSModels;

namespace SMSUI.BaseInfo
{
    public partial class FrmSupplierSetup : Form
    {
        BLL.BaseIndo.SupplierManager supplierManager = new BLL.BaseIndo.SupplierManager();

        public FrmSupplierSetup()
        {
            InitializeComponent();
        }
        //显示提示
        private void txtSupplierName_Enter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtSupplierName, "供货商名称唯一，不可重复");
        }
        //窗体加载
        private void FrmSupplierSetup_Load(object sender, EventArgs e)
        {
            DataSet myds = supplierManager.GetDataSet();
            dgvSupplierInfo.DataSource = myds.Tables["supplierInfo"];
        }
        //退出
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //列表单击
        private void dgvSupplierInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtSupplierID.Text = Convert.ToString(dgvSupplierInfo[0, dgvSupplierInfo.CurrentCell.RowIndex].Value);
            txtSupplierName.Text = Convert.ToString(dgvSupplierInfo[1, dgvSupplierInfo.CurrentCell.RowIndex].Value);
            txtLinkman.Text = Convert.ToString(dgvSupplierInfo[2, dgvSupplierInfo.CurrentCell.RowIndex].Value);
            txtPhone.Text = Convert.ToString(dgvSupplierInfo[3, dgvSupplierInfo.CurrentCell.RowIndex].Value);
            txtFax.Text = Convert.ToString(dgvSupplierInfo[4, dgvSupplierInfo.CurrentCell.RowIndex].Value);
            txtAddress.Text = Convert.ToString(dgvSupplierInfo[5, dgvSupplierInfo.CurrentCell.RowIndex].Value);
            txtRemarks.Text = Convert.ToString(dgvSupplierInfo[6, dgvSupplierInfo.CurrentCell.RowIndex].Value);

        }

        //添加供货商
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtSupplierName.Text.Trim().Length == 0 || txtLinkman.Text.Trim().Length == 0 || txtPhone.Text.Trim().Length == 0 || txtAddress.Text.Trim().Length == 0)
            {
                MessageBox.Show("供货商名称、联系人、联系人电话、地址不能为空！");
            }
            else
            {
                Supplier supplier = new Supplier
                {
                   SupplierName=txtSupplierName.Text,
                   Linkman=txtLinkman.Text,
                   Phone=txtPhone.Text,
                   Fax=txtFax.Text,
                   Address=txtAddress.Text,
                   Remarks=txtRemarks.Text
                };

                if (supplierManager.AddSupplier(supplier) > 0)
                {
                    MessageBox.Show("供货商添加成功！");
                    FrmSupplierSetup_Load(sender,e);
                }
                else
                {
                    MessageBox.Show("供货商添加失败！请检查！");
                }
            }
        }

        //修改供货商信息
        private void btnRevise_Click(object sender, EventArgs e)
        {
            if (txtSupplierName.Text.Trim().Length == 0 || txtLinkman.Text.Trim().Length == 0 || txtPhone.Text.Trim().Length == 0 || txtAddress.Text.Trim().Length == 0)
            {
                MessageBox.Show("供货商名称、联系人、联系人电话、地址不能为空！");
            }
            else
            {
                Supplier supplier = new Supplier
                {
                    SupplierID=Convert.ToInt32(txtSupplierID.Text),
                    SupplierName = txtSupplierName.Text.Trim(),
                    Linkman = txtLinkman.Text.Trim(),
                    Phone = txtPhone.Text.Trim(),
                    Fax = txtFax.Text.Trim(),
                    Address = txtAddress.Text.Trim(),
                    Remarks = txtRemarks.Text.Trim()
                };

                if (supplierManager.UpdateStoreInfo(supplier) > 0)
                {
                    MessageBox.Show("供货商修改成功！");
                    FrmSupplierSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("供货商修改失败！请检查！");
                }
            }
        }

        //删除供货商
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvSupplierInfo.CurrentCell != null && dgvSupplierInfo.CurrentCell.RowIndex != -1)
            {
                Supplier supplier=new Supplier 
                {
                    SupplierID = Convert.ToInt32(dgvSupplierInfo[0, dgvSupplierInfo.CurrentCell.RowIndex].Value)

                };

                if (supplierManager.DeleteSupplier(supplier) > 0)
                {
                    MessageBox.Show("供货商删除成功！");
                    FrmSupplierSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("供货商删除失败！");
                }
            }
            else
            {
                MessageBox.Show("请在列表中选择要删除的供货商！", "提示");
            }
        }
    }
}
