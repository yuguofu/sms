﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using SMSModels;

namespace SMSUI.BaseInfo
{
    public partial class FrmStoreSetup : Form
    {
        BLL.BaseIndo.StoreManager storeManager = new BLL.BaseIndo.StoreManager();
        public FrmStoreSetup()
        {
            InitializeComponent();
        }

        //窗口加载绑定datagridview数据源
        private void FrmStoreSetup_Load(object sender, EventArgs e)
        {
            DataSet myds = storeManager.GetDataSet();
            dgvStoreInfo.DataSource = myds.Tables["storeInfo"];
        }
        //退出
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvStoreInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtStoreID.Text = Convert.ToString(dgvStoreInfo[0, dgvStoreInfo.CurrentCell.RowIndex].Value);
            txtStoreName.Text = Convert.ToString(dgvStoreInfo[1, dgvStoreInfo.CurrentCell.RowIndex].Value);
            txtPIC.Text = Convert.ToString(dgvStoreInfo[2, dgvStoreInfo.CurrentCell.RowIndex].Value);
            txtPhone.Text = Convert.ToString(dgvStoreInfo[3, dgvStoreInfo.CurrentCell.RowIndex].Value);
            txtAddress.Text = Convert.ToString(dgvStoreInfo[4, dgvStoreInfo.CurrentCell.RowIndex].Value);
            txtRemarks.Text = Convert.ToString(dgvStoreInfo[5, dgvStoreInfo.CurrentCell.RowIndex].Value);
        }

        //添加仓库
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtStoreName.Text.Trim().Length == 0 || txtPIC.Text.Trim().Length == 0 || txtPhone.Text.Trim().Length == 0 || txtAddress.Text.Trim().Length == 0)
            {
                MessageBox.Show("仓库名称、负责人、负责人电话、地址不能为空！");
            }
            else
            {
                Store store = new Store
                {
                    StoreName = txtStoreName.Text.Trim(),
                    PIC = txtPIC.Text.Trim(),
                    Phone = txtPhone.Text.Trim(),
                    Address = txtAddress.Text,
                    Remarks = txtRemarks.Text.Trim()
                };

                if (storeManager.AddStore(store) > 0)
                {
                    MessageBox.Show("仓库添加成功！");
                    FrmStoreSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("仓库添加失败！请检查！");
                }
            }
        }

        //修改仓库信息
        private void btnRevise_Click(object sender, EventArgs e)
        {
            if (txtStoreName.Text.Trim().Length == 0 || txtPIC.Text.Trim().Length == 0 || txtPhone.Text.Trim().Length == 0 || txtAddress.Text.Trim().Length == 0)
            {
                MessageBox.Show("仓库名称、负责人、负责人电话、地址不能为空！");
            }
            else
            {
                Store store = new Store
                {
                    StoreID = Convert.ToInt32(txtStoreID.Text),
                    StoreName = txtStoreName.Text.Trim(),
                    PIC = txtPIC.Text.Trim(),
                    Phone = txtPhone.Text.Trim(),
                    Address = txtAddress.Text,
                    Remarks = txtRemarks.Text.Trim()
                };

                if (storeManager.UpdateStoreInfo(store) > 0)
                {
                    MessageBox.Show("仓库修改成功！");
                    FrmStoreSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("仓库修改失败！请检查！");
                }
            }
        }

        //删除仓库
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvStoreInfo.CurrentCell != null && dgvStoreInfo.CurrentCell.RowIndex != -1)
            {
                Store store = new Store
                {
                    StoreID = Convert.ToInt32(dgvStoreInfo[0, dgvStoreInfo.CurrentCell.RowIndex].Value)

                };

                if (storeManager.DeleteStore(store) > 0)
                {
                    MessageBox.Show("仓库删除成功！");
                    FrmStoreSetup_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("仓库删除失败！");
                }
            }
            else
            {
                MessageBox.Show("请在列表中选择要删除的仓库！","提示");
            }
        }
        //显示提示
        private void txtStoreName_Enter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(txtStoreName, "仓库名唯一，不可重复");
        }
    }
}
