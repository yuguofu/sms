﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSUI
{
    public partial class FrmMain : Form
    {
        
        public FrmMain()
        {
            InitializeComponent();
        }
        bool isMouseDown = false;
        Point currentFormLocation = new Point(); //当前窗体位置
        Point currentMouseOffset = new Point(); //当前鼠标的按下位置
        
        //窗口描边
        private void FrmMain_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.PowderBlue, 0, 0, this.Width - 1, this.Height - 1);

        }
        //窗口拖动
        private void PnlMainTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                currentFormLocation = this.Location;
                currentMouseOffset = Control.MousePosition;
            }
        }
        //窗口拖动
        private void PnlMainTitle_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }
        //窗口拖动
        private void PnlMainTitle_MouseMove(object sender, MouseEventArgs e)
        {
            int rangeX = 0, rangeY = 0; //计算当前鼠标光标的位移，让窗体进行相同大小的位移
            if (isMouseDown)
            {
                Point pt = Control.MousePosition;
                rangeX = currentMouseOffset.X - pt.X;
                rangeY = currentMouseOffset.Y - pt.Y;
                this.Location = new Point(currentFormLocation.X - rangeX, currentFormLocation.Y - rangeY);
            }
        }
        //退出
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //最小化
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //窗口加载
        private void FrmMain_Load(object sender, EventArgs e)
        {
            if (!System.IO.File.Exists("./出库方式配置.ini"))
            {
                IniFile IniFile = new IniFile("./出库方式配置.ini");
                IniFile.IniWriteValue("出库方式", "0", "零售");
                IniFile.IniWriteValue("出库方式", "1", "批发");
            }


            if (Program.currentUser.Power != 0)
            {
                picUserManager.Enabled = false;
                picUserManager.Image = SMSUI.Properties.Resources.用户管理1;
            }
            toolStripStatusCurrentUser.Text += Program.currentUser.UserName + "("+ Program.currentUser.UserType + ") |";
            //toolStripStatusCurrentUser.Text += Program.currentUser.UserName+ " |";
            toolStripStatusLoginTime.Text += DateTime.Now.ToString() + " |";
            tmrCurrentTime.Start();
        }
        //现行时间
        private void tmrCurrentTime_Tick(object sender, EventArgs e)
        {
            toolStripStatusCurrentTime.Text = "现行时间：" + DateTime.Now.ToString() + " |";
        }


        //仓库设置
        private void picStoreSetup_Click(object sender, EventArgs e)
        {
            BaseInfo.FrmStoreSetup frmStoreSetup = new BaseInfo.FrmStoreSetup();
            frmStoreSetup.ShowDialog();
            
        }
        //供货商设置
        private void picSupplierSetup_Click(object sender, EventArgs e)
        {
            BaseInfo.FrmSupplierSetup frmSupplierSetup = new BaseInfo.FrmSupplierSetup();
            frmSupplierSetup.ShowDialog();
        }
        //货物档案设置
        private void picGoodsInfoSetup_Click(object sender, EventArgs e)
        {
            BaseInfo.FrmGoodsFilesSetup frmGoodsFilesSetup = new BaseInfo.FrmGoodsFilesSetup();
            frmGoodsFilesSetup.ShowDialog();
        }
        //入库管理
        private void picInStorageManager_Click(object sender, EventArgs e)
        {
            GoodsManager.FrmInStoreManager frmInStorageManager = new GoodsManager.FrmInStoreManager();
            frmInStorageManager.ShowDialog();
        }
        //出库管理
        private void picOutStoreManager_Click(object sender, EventArgs e)
        {
            GoodsManager.FrmOutStoreManager frmOutStoreManager = new GoodsManager.FrmOutStoreManager();
            frmOutStoreManager.ShowDialog();
        }
        //借货管理
        private void picBorrowGoods_Click(object sender, EventArgs e)
        {
            GoodsManager.FrmBorrowGoods frmBorrowGoods = new GoodsManager.FrmBorrowGoods();
            frmBorrowGoods.ShowDialog();

        }
        //还货管理
        private void picReturnGoods_Click(object sender, EventArgs e)
        {
            GoodsManager.FrmReturnGoods frmReturnGoods = new GoodsManager.FrmReturnGoods();
            frmReturnGoods.ShowDialog();
        }
        //入库查询
        private void picInStorageQuery_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmInStorageQuery frmInStorageQuery = new QueryStatistics.FrmInStorageQuery();
            frmInStorageQuery.ShowDialog();
        }
        //出库查询
        private void PicOutStoreQuery_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmOutStoreQuery frmOutStoreQuery = new QueryStatistics.FrmOutStoreQuery();
            frmOutStoreQuery.ShowDialog();
        }
        //库存查询
        private void PicStockQuery_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmStockQuery frmStockQuery = new QueryStatistics.FrmStockQuery();
            frmStockQuery.ShowDialog();
        }
        //借货查询
        private void PicBorrowGoodsQuery_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmBorrowGoodsQuery frmBorrowGoodsQuery = new QueryStatistics.FrmBorrowGoodsQuery();
            frmBorrowGoodsQuery.ShowDialog();
        }
        //还货查询
        private void PicReturnGoodsQuery_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmReturnGoodsQuery frmReturnGoodsQuery = new QueryStatistics.FrmReturnGoodsQuery();
            frmReturnGoodsQuery.ShowDialog();
        }
        //出库月统计
        private void PicOutStoreMonthlyStatistics_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmOutStoreMonthlyStatistics frmOutStoreMonthlyStatistics = new QueryStatistics.FrmOutStoreMonthlyStatistics();
            frmOutStoreMonthlyStatistics.ShowDialog();
        }
        //出库年统计
        private void PicOutStoreYearStatistics_Click(object sender, EventArgs e)
        {
            QueryStatistics.FrmOutStoreYearStatistics frmOutStoreYearStatistics = new QueryStatistics.FrmOutStoreYearStatistics();
            frmOutStoreYearStatistics.ShowDialog();
        }
        //用户管理
        private void PicUserManager_Click(object sender, EventArgs e)
        {
            Other.FrmUserManager frmUserManager = new Other.FrmUserManager();
            frmUserManager.ShowDialog();
            
        }
        //关于
        private void picAbout_Click(object sender, EventArgs e)
        {
            Other.FrmAbout frmAbout = new Other.FrmAbout();
            frmAbout.ShowDialog();
        }
        //退出
        private void PicExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //退出前确认
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("确定退出系统吗？","提示",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
