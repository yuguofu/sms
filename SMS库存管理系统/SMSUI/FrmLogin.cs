﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SMSModels;
using BLL;

namespace SMSUI
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        //实例化用户管理类，以备调用登录方法
        BLL.BaseIndo.UserManager userManager = new BLL.BaseIndo.UserManager();

        bool isMouseDown = false;
        Point currentFormLocation = new Point(); //当前窗体位置
        Point currentMouseOffset = new Point(); //当前鼠标的按下位置

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            Console.WriteLine(System.AppDomain.CurrentDomain.BaseDirectory); 
            Console.WriteLine(Application.StartupPath);
        }

        /// <summary>
        /// 绘制边框颜色
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLogin_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.PaleTurquoise , 0, 0, this.Width - 1, this.Height - 1);
        }
        
        
        #region 窗口拖动
        private void LblTItle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                currentFormLocation = this.Location;
                currentMouseOffset = Control.MousePosition;
            }
        }

        private void LblTItle_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void LblTItle_MouseMove(object sender, MouseEventArgs e)
        {
            int rangeX = 0, rangeY = 0; //计算当前鼠标光标的位移，让窗体进行相同大小的位移
            if (isMouseDown)
            {
                Point pt = Control.MousePosition;
                rangeX = currentMouseOffset.X - pt.X;
                rangeY = currentMouseOffset.Y - pt.Y;
                this.Location = new Point(currentFormLocation.X - rangeX, currentFormLocation.Y - rangeY);
            }
        }
        #endregion

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #region 窗口拖动
        private void LblLoginTitle_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;
                currentFormLocation = this.Location;
                currentMouseOffset = Control.MousePosition;
            }
        }

        private void LblLoginTitle_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void LblLoginTitle_MouseMove(object sender, MouseEventArgs e)
        {
            int rangeX = 0, rangeY = 0; //计算当前鼠标光标的位移，让窗体进行相同大小的位移
            if (isMouseDown)
            {
                Point pt = Control.MousePosition;
                rangeX = currentMouseOffset.X - pt.X;
                rangeY = currentMouseOffset.Y - pt.Y;
                this.Location = new Point(currentFormLocation.X - rangeX, currentFormLocation.Y - rangeY);
            }
        }
        #endregion

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            //【1】验证输入
            if (txtUserName.Text.Trim().Length == 0 && txtPassword.Text.Trim().Length == 0)
            {
                MessageBox.Show("用户名、密码不能为空！", "提示");
                return;

            }

            //【2】封装user作为参数
            User user = new User()
            {
                UserName = txtUserName.Text.Trim(),
                Password = txtPassword.Text.Trim(),
                
            };

            //【3】调用登录逻辑
            user = userManager.UserLogin(user);

            //【4】验证登录是否成功
            if (user!=null)
            {
                //登录成功保存当前用户对象信息
                Program.currentUser = user;
                //设置窗口返回值
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("用户名或密码错误！", "提示");
            }
        }

        private void TxtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                BtnLogin_Click(sender,e);
            }
        }

        private void TxtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtPassword.Focus();
            }
            
        }
    }
}
