﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using SMSModels;
using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;

namespace SMSUI.GoodsManager
{
    public partial class FrmBorrowGoods : Form
    {
        private BLL.GoodsManager.BorrowGoodsManager borrowGoodsManager = new BLL.GoodsManager.BorrowGoodsManager();
        private BLL.BaseIndo.GoodsFilesManager goodsFilesManager = new BLL.BaseIndo.GoodsFilesManager();
        private BLL.BaseIndo.StoreManager storeManager = new BLL.BaseIndo.StoreManager();
        DataSet gfds;
        DataSet sds;
        List<string> printList = new List<string>();
        public FrmBorrowGoods()
        {
            InitializeComponent();
            if (Program.currentUser.Power != 0)     //非管理员禁用 修改、删除
            {
                btnRevise.Enabled = false;
                btnDelete.Enabled = false;
            }
            gfds = goodsFilesManager.GetDataSet();  //获取货物档案dataset
            sds = storeManager.GetDataSet();  //获取仓库信息dataset
        }


        //窗体加载
        private void FrmBorrowGoods_Load(object sender, EventArgs e)
        {
            //List<Goods> list = goodsFilesManager.GetGoodsInfo();

            DataSet myds = borrowGoodsManager.GetDataSetFromBorrowGoodsTable();
            dgvBorrowGoodsInfo.DataSource = myds.Tables[0];

            //绑定货物编号组合框数据
            cmbGoodsNum.DataSource = gfds.Tables[0];
            cmbGoodsNum.DisplayMember = "货物编号";
            cmbGoodsNum.SelectedIndex = -1;
            //绑定货物名称组合框数据
            cmbGoodsName.DataSource = gfds.Tables[0];
            cmbGoodsName.DisplayMember = "货物名称";
            cmbGoodsName.SelectedIndex = -1;
            //绑定仓库名称组合框数据
            cmbStoreName.DataSource = sds.Tables[0];
            cmbStoreName.DisplayMember = "仓库名称";
            cmbStoreName.ValueMember = "仓库ID";
            cmbStoreName.SelectedIndex = -1;
            //绑定规格组合框数据
            cmbSpe.DataSource = gfds.Tables[0];
            cmbSpe.DisplayMember = "规格";
            cmbSpe.SelectedIndex = -1;
            //绑定计量单位组合框数据
            cmbUnits.DataSource = gfds.Tables[0];
            cmbUnits.DisplayMember = "计量单位";
            cmbUnits.SelectedIndex = -1;

            DeleDOC();
        }

        //列表单击
        private void dgvBorrowGoodsInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtBorrowGoodsUnits.Text = dgvBorrowGoodsInfo[1,dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();   //借货单位
            txtBorrower.Text= dgvBorrowGoodsInfo[2, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();   //借货人
            txtPhone.Text= dgvBorrowGoodsInfo[3, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();   //联系电话
            cmbGoodsNum.Text = dgvBorrowGoodsInfo[4, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //货物编号
            cmbGoodsName.Text = dgvBorrowGoodsInfo[5, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //货物名称
            cmbStoreName.Text = dgvBorrowGoodsInfo[7, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //仓库名称
            //Console.WriteLine(cmbStoreName.SelectedValue);
            cmbSpe.Text = dgvBorrowGoodsInfo[8, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //规格
            cmbUnits.Text = dgvBorrowGoodsInfo[9, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //计量单位
            txtBorrowGoodsQuantity.Text= dgvBorrowGoodsInfo[10, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //借货数量
            txtHandler.Text = dgvBorrowGoodsInfo[11, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //经手人
            txtRemarks.Text = dgvBorrowGoodsInfo[13, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString();  //备注

        }

        //退出
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //借货
        private void btnBorrowGoods_Click(object sender, EventArgs e)
        {
            if (txtBorrowGoodsUnits.Text.Trim().Length==0 || txtBorrower.Text.Trim().Length == 0 || txtPhone.Text.Trim().Length == 0 || cmbGoodsNum.Text.Length == 0 || cmbGoodsName.Text.Length == 0 || cmbStoreName.Text.Length == 0 || cmbSpe.Text.Length == 0 || cmbUnits.Text.Length == 0 || txtBorrowGoodsQuantity.Text.Trim().Length == 0 || txtHandler.Text.Trim().Length == 0)
            {
                MessageBox.Show("请填写完整！");
            }
            else
            {
                BorrowGoods borrowGoods = new BorrowGoods
                {
                    BorrowGoodsID = "jh" + DateTime.Now.ToString("yyyyMMddHHmmss"), //借货编号
                    BorrowGoodsUnits = txtBorrowGoodsUnits.Text.Trim(),  //借货单位
                    Borrower=txtBorrower.Text.Trim(),  //借货人
                    BorrowerPhone=txtPhone.Text.Trim(),   //电话
                    GoodsID = cmbGoodsNum.Text,  //货物编号
                    StoreID = Convert.ToInt32(cmbStoreName.SelectedValue),  //仓库ID
                    BorrowGoodsQuantity = Convert.ToInt32(txtBorrowGoodsQuantity.Text.Trim()),  //借货数量
                    BorrowGoodsTime = DateTime.Now.ToString("yyyy年MM月dd日HH时mm分"),  //借货时间
                    Handler = txtHandler.Text.Trim(),  //经手人
                    Remarks = txtRemarks.Text.Trim()  //备注
                };

                if (borrowGoodsManager.IsEnoughOfBorrowGoods(borrowGoods))   //货物存在于库存中且数量充足
                {
                    if (borrowGoodsManager.BorrowGoods(borrowGoods) > 0 && borrowGoodsManager.UpdateStorageOnBorrowGoods(borrowGoods) > 0)
                    {
                        MessageBox.Show("借货成功！");
                        FrmBorrowGoods_Load(sender, e);
                    }
                    else
                    {
                        MessageBox.Show("借货失败！请检查！");
                    }
                }
                else     //
                {
                    MessageBox.Show("库存中该货物数量不足！请检查！");
                }
            }
        }

        //修改
        private void btnRevise_Click(object sender, EventArgs e)
        {
            if (txtBorrowGoodsUnits.Text.Trim().Length == 0 || txtBorrower.Text.Trim().Length == 0 || txtPhone.Text.Trim().Length == 0 || cmbGoodsNum.Text.Length == 0 || cmbGoodsName.Text.Length == 0 || cmbStoreName.Text.Length == 0 || cmbSpe.Text.Length == 0 || cmbUnits.Text.Length == 0 || txtBorrowGoodsQuantity.Text.Trim().Length == 0 || txtHandler.Text.Trim().Length == 0)
            {
                MessageBox.Show("请填写完整！");
            }
            else
            {
                BorrowGoods borrowGoods = new BorrowGoods
                {
                    BorrowGoodsID = dgvBorrowGoodsInfo[0,dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), //借货编号
                    BorrowGoodsUnits = txtBorrowGoodsUnits.Text.Trim(),  //借货单位
                    Borrower = txtBorrower.Text.Trim(),  //借货人
                    BorrowerPhone = txtPhone.Text.Trim(),   //电话
                    GoodsID = cmbGoodsNum.Text,  //货物编号
                    StoreID = Convert.ToInt32(cmbStoreName.SelectedValue),  //仓库ID
                    BorrowGoodsQuantity = Convert.ToInt32(txtBorrowGoodsQuantity.Text.Trim()),  //借货数量
                    BorrowGoodsTime = DateTime.Now.ToString("yyyy年MM月dd日HH时mm分"),  //借货时间
                    Handler = txtHandler.Text.Trim(),  //经手人
                    Remarks = txtRemarks.Text.Trim()  //备注
                };

                if (borrowGoodsManager.ReviseBorrowGoodsInfo(borrowGoods) >0)
                {
                    MessageBox.Show("修改成功！");
                    FrmBorrowGoods_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("修改失败！请检查！");
                }
            }
        }

        //删除
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvBorrowGoodsInfo.CurrentCell != null && dgvBorrowGoodsInfo.CurrentCell.RowIndex != -1)
            {
                BorrowGoods borrowGoods = new BorrowGoods
                {
                    BorrowGoodsID = dgvBorrowGoodsInfo[0, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString()

                };

                if (borrowGoodsManager.DeleteBorrowGoodsInfo(borrowGoods) > 0)
                {
                    MessageBox.Show("删除成功！");
                    FrmBorrowGoods_Load(sender, e);
                }
                else
                {
                    MessageBox.Show("删除失败！");
                }
            }
            else
            {
                MessageBox.Show("请在列表中选择要删除的记录！", "提示");
            }
        }


        //货物编号选择事件
        private void cmbGoodsNum_SelectionChangeCommitted(object sender, EventArgs e)
        {

            //cmbGoodsName.Text = list[0].GoodsName;  //货物名称
            //cmbStoreName.Text = list[0].StoreName;  //仓库名称
            //cmbSpe.Text = list[0].Spec;   //规格
            //cmbUnits.Text = list[0].Units;  //计量单位
        }
        //弹出添加到待打印菜单
        private void dgvBorrowGoodsInfo_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button==MouseButtons.Right)
            {
                cmsAddPrint.Show(MousePosition.X,MousePosition.Y);
            }
        }
        //添加到待打印
        private void 添加到待打印ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string printNote = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#{10}", dgvBorrowGoodsInfo[0, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[1, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[2, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[3, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[4, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[5, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[7, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[8, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[9, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[10, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString(), dgvBorrowGoodsInfo[11, dgvBorrowGoodsInfo.CurrentCell.RowIndex].Value.ToString());
            if (printList.Contains(printNote))  //待打印记录已存在
            {
                return;
            }
            else
            {
                if (printList.Count >= 10)
                {
                    MessageBox.Show("一次只能添加打印10条记录！默认A4纸，太多放不下！");
                    return;
                }
                printList.Add(printNote);
                btnPrint.Text = "打印 " + printList.Count + "条";

            }
        }
        //清空待打印
        private void 清空待打印ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printList.Clear();
            btnPrint.Text = "打印";
        }
        //打印按钮单击
        private void btnPrint_Click(object sender, EventArgs e)
        {
            //删除存在的入库单
            DeleDOC();
            //写入表格数据
            if (printList.Count == 0)
            {
                MessageBox.Show("请在列表中右键添加待打印的借货记录！");
            }
            else
            {
                #region 创建待打印word
                //创建一个文档对象
                Spire.Doc.Document doc = new Document();
                //添加一个section
                Section section = doc.AddSection();

                //纸张横向
                Section sec = doc.Sections[0];
                sec.PageSetup.Orientation = PageOrientation.Landscape;

                //添加标题
                Paragraph inStoreTitle = section.AddParagraph();
                inStoreTitle.AppendText("借货单");
                //设置标题格式
                ParagraphStyle style1 = new ParagraphStyle(doc);
                style1.Name = "titleStyle";
                style1.CharacterFormat.Bold = true;
                style1.CharacterFormat.TextColor = Color.Black;
                style1.CharacterFormat.FontName = "方正小标宋简体";
                style1.CharacterFormat.FontSize = 24f;
                doc.Styles.Add(style1);
                inStoreTitle.ApplyStyle("titleStyle");
                //标题居中
                inStoreTitle.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;
                //添加单号
                Paragraph pStrat = section.AddParagraph();
                pStrat.AppendText("单号：jhd" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                //设置单号格式
                ParagraphStyle style12 = new ParagraphStyle(doc);
                style12.Name = "oddNumStyle";
                style12.CharacterFormat.Bold = false;
                style12.CharacterFormat.TextColor = Color.Black;
                style12.CharacterFormat.FontName = "宋体";
                style12.CharacterFormat.FontSize = 12f;
                doc.Styles.Add(style12);
                pStrat.ApplyStyle("oddNumStyle");

                //添加表格
                //Section section = doc.AddSection(); //添加section
                Table table = section.AddTable(true);
                //指定表格的行数和列数（11行，11列）
                table.ResetCells(11, 11);

                //标题行数组
                string[] tabTitle = { "借货编号", "借货单位", "借货人", "联系电话", "货物编号", "货物名称", "所属仓库", "规格", "计量单位", "借货数量", "经手人" };
                //添加表格标题行
                for (int i = 0; i < 1; i++)
                {
                    for (int j = 0; j < 11; j++)
                    {
                        TextRange range = table[i, j].AddParagraph().AppendText(tabTitle[j]);
                        range.CharacterFormat.FontName = "黑体";
                        range.CharacterFormat.FontSize = 12;
                        range.CharacterFormat.TextColor = Color.Black;
                        range.CharacterFormat.Bold = false;
                        table[i, j].Paragraphs[0].Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Center;  //水平居中
                        table[i, j].CellFormat.VerticalAlignment = VerticalAlignment.Middle;    //垂直居中
                    }
                }

                //遍历打印列表并插入表格数据
                for (int i = 0; i < printList.Count; i++)
                {
                    string[] arr = printList[i].Split('#');
                    for (int j = 0; j < 11; j++)
                    {
                        TextRange range = table[i + 1, j].AddParagraph().AppendText(arr[j]);
                        range.CharacterFormat.FontName = "宋体";
                        range.CharacterFormat.FontSize = 10;
                        range.CharacterFormat.TextColor = Color.Black;
                        range.CharacterFormat.Bold = false;
                        table[i + 1, j].CellFormat.VerticalAlignment = VerticalAlignment.Middle;    //垂直居中
                    }
                }
                //添加签字栏
                Paragraph pEnd = section.AddParagraph();
                pEnd.AppendText("制单人：              审核人：              日期：    年  月  日");
                //设置签字栏格式
                ParagraphStyle style13 = new ParagraphStyle(doc);
                style13.Name = "endLineStyle";
                style13.CharacterFormat.Bold = false;
                style13.CharacterFormat.TextColor = Color.Black;
                style13.CharacterFormat.FontName = "宋体";
                style13.CharacterFormat.FontSize = 12f;
                doc.Styles.Add(style13);
                pEnd.ApplyStyle("endLineStyle");

                //保存文档
                doc.SaveToFile("./borrowGoodsDoc.docx", FileFormat.Docx2013);
                #endregion

                System.Threading.Thread.Sleep(1000);

                #region 开始打印
                //初始化PrintDialog实例
                PrintDialog dialog = new PrintDialog();

                //设置打印对话框属性
                dialog.AllowPrintToFile = true;
                dialog.AllowCurrentPage = true;
                dialog.AllowSomePages = true;

                //设置文档打印对话框
                doc.PrintDialog = dialog;

                //显示打印对话框并点击确定执行打印
                System.Drawing.Printing.PrintDocument printDoc = doc.PrintDocument;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    printDoc.Print();
                }
                DeleDOC();
                #endregion
            }
        }
        //弹出清空待打印菜单
        private void btnPrint_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                cmsClearPrintList.Show(MousePosition.X, MousePosition.Y);
            }
        }

        /// <summary>
        /// 删除borrowGoodsDoc.docx文件
        /// </summary>
        public void DeleDOC()
        {
            if (System.IO.File.Exists("./borrowGoodsDoc.docx"))
            {
                System.IO.File.Delete("./borrowGoodsDoc.docx");
            }
        }
    }
}
