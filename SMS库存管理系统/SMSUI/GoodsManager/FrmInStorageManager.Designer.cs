﻿namespace SMSUI.GoodsManager
{
    partial class FrmInStoreManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtHandler = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtInStoreQuantity = new System.Windows.Forms.TextBox();
            this.cmbSupplierName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbInStoreMonovalent = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbSpe = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbStoreName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbGoodsName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbGoodsNum = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cmsClearPrintList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.清空待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnRevise = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnInStorage = new System.Windows.Forms.Button();
            this.dgvInStoreInfo = new System.Windows.Forms.DataGridView();
            this.cmsAddPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加到待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.cmsClearPrintList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInStoreInfo)).BeginInit();
            this.cmsAddPrint.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.txtHandler);
            this.groupBox1.Controls.Add(this.txtTotal);
            this.groupBox1.Controls.Add(this.txtInStoreQuantity);
            this.groupBox1.Controls.Add(this.cmbSupplierName);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cmbInStoreMonovalent);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cmbUnits);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cmbSpe);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmbStoreName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cmbGoodsName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cmbGoodsNum);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(812, 296);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "信息";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(435, 256);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(349, 19);
            this.label12.TabIndex = 12;
            this.label12.Text = "提示：右键列表添加待打印，右键打印按钮清空待打印。";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(159, 250);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(4);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(200, 25);
            this.txtRemarks.TabIndex = 11;
            // 
            // txtHandler
            // 
            this.txtHandler.Location = new System.Drawing.Point(543, 204);
            this.txtHandler.Margin = new System.Windows.Forms.Padding(4);
            this.txtHandler.Name = "txtHandler";
            this.txtHandler.Size = new System.Drawing.Size(200, 25);
            this.txtHandler.TabIndex = 10;
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(159, 200);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(200, 25);
            this.txtTotal.TabIndex = 9;
            // 
            // txtInStoreQuantity
            // 
            this.txtInStoreQuantity.Location = new System.Drawing.Point(543, 155);
            this.txtInStoreQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.txtInStoreQuantity.Name = "txtInStoreQuantity";
            this.txtInStoreQuantity.Size = new System.Drawing.Size(200, 25);
            this.txtInStoreQuantity.TabIndex = 8;
            this.txtInStoreQuantity.TextChanged += new System.EventHandler(this.txtInStoreQuantity_TextChanged);
            // 
            // cmbSupplierName
            // 
            this.cmbSupplierName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSupplierName.FormattingEnabled = true;
            this.cmbSupplierName.Location = new System.Drawing.Point(543, 68);
            this.cmbSupplierName.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSupplierName.Name = "cmbSupplierName";
            this.cmbSupplierName.Size = new System.Drawing.Size(200, 23);
            this.cmbSupplierName.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(64, 254);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 15);
            this.label11.TabIndex = 4;
            this.label11.Text = "备    注：";
            // 
            // cmbInStoreMonovalent
            // 
            this.cmbInStoreMonovalent.FormattingEnabled = true;
            this.cmbInStoreMonovalent.Location = new System.Drawing.Point(159, 156);
            this.cmbInStoreMonovalent.Margin = new System.Windows.Forms.Padding(4);
            this.cmbInStoreMonovalent.Name = "cmbInStoreMonovalent";
            this.cmbInStoreMonovalent.Size = new System.Drawing.Size(200, 23);
            this.cmbInStoreMonovalent.TabIndex = 7;
            this.cmbInStoreMonovalent.TextChanged += new System.EventHandler(this.cmbInStoreMonovalent_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(455, 211);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "经 手 人：";
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Location = new System.Drawing.Point(543, 114);
            this.cmbUnits.Margin = new System.Windows.Forms.Padding(4);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(200, 23);
            this.cmbUnits.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(64, 208);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 15);
            this.label9.TabIndex = 4;
            this.label9.Text = "总 金 额：";
            // 
            // cmbSpe
            // 
            this.cmbSpe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpe.FormattingEnabled = true;
            this.cmbSpe.Location = new System.Drawing.Point(159, 110);
            this.cmbSpe.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSpe.Name = "cmbSpe";
            this.cmbSpe.Size = new System.Drawing.Size(200, 23);
            this.cmbSpe.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(455, 164);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 4;
            this.label8.Text = "入库数量：";
            // 
            // cmbStoreName
            // 
            this.cmbStoreName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStoreName.FormattingEnabled = true;
            this.cmbStoreName.Location = new System.Drawing.Point(159, 68);
            this.cmbStoreName.Margin = new System.Windows.Forms.Padding(4);
            this.cmbStoreName.Name = "cmbStoreName";
            this.cmbStoreName.Size = new System.Drawing.Size(200, 23);
            this.cmbStoreName.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(64, 160);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "入库单价：";
            // 
            // cmbGoodsName
            // 
            this.cmbGoodsName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsName.FormattingEnabled = true;
            this.cmbGoodsName.Location = new System.Drawing.Point(543, 25);
            this.cmbGoodsName.Margin = new System.Windows.Forms.Padding(4);
            this.cmbGoodsName.Name = "cmbGoodsName";
            this.cmbGoodsName.Size = new System.Drawing.Size(200, 23);
            this.cmbGoodsName.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(455, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "计量单位：";
            // 
            // cmbGoodsNum
            // 
            this.cmbGoodsNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsNum.FormattingEnabled = true;
            this.cmbGoodsNum.Location = new System.Drawing.Point(159, 25);
            this.cmbGoodsNum.Margin = new System.Windows.Forms.Padding(4);
            this.cmbGoodsNum.MaxDropDownItems = 10;
            this.cmbGoodsNum.Name = "cmbGoodsNum";
            this.cmbGoodsNum.Size = new System.Drawing.Size(200, 23);
            this.cmbGoodsNum.TabIndex = 1;
            this.cmbGoodsNum.SelectionChangeCommitted += new System.EventHandler(this.cmbGoodsNum_SelectionChangeCommitted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(64, 115);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "规    格：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(455, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "供 应 商：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "仓库名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(455, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "货物名称：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "货物编号：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnPrint);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.btnRevise);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnInStorage);
            this.groupBox2.Location = new System.Drawing.Point(865, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(189, 296);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "操作";
            // 
            // btnPrint
            // 
            this.btnPrint.ContextMenuStrip = this.cmsClearPrintList;
            this.btnPrint.Location = new System.Drawing.Point(44, 180);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 29);
            this.btnPrint.TabIndex = 16;
            this.btnPrint.Text = "打印";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnPrint_MouseClick);
            // 
            // cmsClearPrintList
            // 
            this.cmsClearPrintList.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsClearPrintList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空待打印ToolStripMenuItem});
            this.cmsClearPrintList.Name = "cmsClearPrintList";
            this.cmsClearPrintList.Size = new System.Drawing.Size(154, 28);
            // 
            // 清空待打印ToolStripMenuItem
            // 
            this.清空待打印ToolStripMenuItem.Name = "清空待打印ToolStripMenuItem";
            this.清空待打印ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.清空待打印ToolStripMenuItem.Text = "清空待打印";
            this.清空待打印ToolStripMenuItem.Click += new System.EventHandler(this.清空待打印ToolStripMenuItem_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(44, 230);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 29);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "退出";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRevise
            // 
            this.btnRevise.Location = new System.Drawing.Point(44, 80);
            this.btnRevise.Margin = new System.Windows.Forms.Padding(4);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(100, 29);
            this.btnRevise.TabIndex = 13;
            this.btnRevise.Text = "修改";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(44, 129);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnInStorage
            // 
            this.btnInStorage.Location = new System.Drawing.Point(44, 31);
            this.btnInStorage.Margin = new System.Windows.Forms.Padding(4);
            this.btnInStorage.Name = "btnInStorage";
            this.btnInStorage.Size = new System.Drawing.Size(100, 29);
            this.btnInStorage.TabIndex = 12;
            this.btnInStorage.Text = "入库";
            this.btnInStorage.UseVisualStyleBackColor = true;
            this.btnInStorage.Click += new System.EventHandler(this.btnInStorage_Click);
            // 
            // dgvInStoreInfo
            // 
            this.dgvInStoreInfo.AllowUserToAddRows = false;
            this.dgvInStoreInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvInStoreInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInStoreInfo.ContextMenuStrip = this.cmsAddPrint;
            this.dgvInStoreInfo.Location = new System.Drawing.Point(16, 319);
            this.dgvInStoreInfo.Margin = new System.Windows.Forms.Padding(4);
            this.dgvInStoreInfo.Name = "dgvInStoreInfo";
            this.dgvInStoreInfo.ReadOnly = true;
            this.dgvInStoreInfo.RowHeadersWidth = 51;
            this.dgvInStoreInfo.RowTemplate.Height = 23;
            this.dgvInStoreInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInStoreInfo.Size = new System.Drawing.Size(1037, 236);
            this.dgvInStoreInfo.TabIndex = 16;
            this.dgvInStoreInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInStoreInfo_CellClick);
            this.dgvInStoreInfo.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvInStoreInfo_CellMouseClick);
            // 
            // cmsAddPrint
            // 
            this.cmsAddPrint.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsAddPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加到待打印ToolStripMenuItem});
            this.cmsAddPrint.Name = "cms";
            this.cmsAddPrint.Size = new System.Drawing.Size(211, 56);
            // 
            // 添加到待打印ToolStripMenuItem
            // 
            this.添加到待打印ToolStripMenuItem.Name = "添加到待打印ToolStripMenuItem";
            this.添加到待打印ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.添加到待打印ToolStripMenuItem.Text = "添加到待打印";
            this.添加到待打印ToolStripMenuItem.Click += new System.EventHandler(this.添加到待打印ToolStripMenuItem_Click);
            // 
            // FrmInStoreManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1067, 564);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvInStoreInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmInStoreManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "入库管理";
            this.Load += new System.EventHandler(this.FrmInStoreManager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.cmsClearPrintList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInStoreInfo)).EndInit();
            this.cmsAddPrint.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbGoodsName;
        private System.Windows.Forms.ComboBox cmbGoodsNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvInStoreInfo;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnInStorage;
        private System.Windows.Forms.ComboBox cmbStoreName;
        private System.Windows.Forms.ComboBox cmbSupplierName;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TextBox txtHandler;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtInStoreQuantity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbInStoreMonovalent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbSpe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.ContextMenuStrip cmsAddPrint;
        private System.Windows.Forms.ToolStripMenuItem 添加到待打印ToolStripMenuItem;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ContextMenuStrip cmsClearPrintList;
        private System.Windows.Forms.ToolStripMenuItem 清空待打印ToolStripMenuItem;
        private System.Windows.Forms.Label label12;
    }
}