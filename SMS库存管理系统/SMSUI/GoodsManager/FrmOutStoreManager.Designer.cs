﻿namespace SMSUI.GoodsManager
{
    partial class FrmOutStoreManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtHandler = new System.Windows.Forms.TextBox();
            this.txtConsignee = new System.Windows.Forms.TextBox();
            this.txtPickUpUnit = new System.Windows.Forms.TextBox();
            this.txtOutStoreTotal = new System.Windows.Forms.TextBox();
            this.txtOutStoreQuantity = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtOutStoreMonovalent = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbSupplierName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbOutStoreType = new System.Windows.Forms.ComboBox();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbSpe = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbStoreName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbGoodsName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbGoodsID = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cmsClearPrintList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.清空待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnRevise = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnOutStorage = new System.Windows.Forms.Button();
            this.dgvOutStoreInfo = new System.Windows.Forms.DataGridView();
            this.cmsAddPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加到待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.cmsClearPrintList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutStoreInfo)).BeginInit();
            this.cmsAddPrint.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.txtHandler);
            this.groupBox1.Controls.Add(this.txtConsignee);
            this.groupBox1.Controls.Add(this.txtPickUpUnit);
            this.groupBox1.Controls.Add(this.txtOutStoreTotal);
            this.groupBox1.Controls.Add(this.txtOutStoreQuantity);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtOutStoreMonovalent);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cmbSupplierName);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cmbOutStoreType);
            this.groupBox1.Controls.Add(this.cmbUnits);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cmbSpe);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmbStoreName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cmbGoodsName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cmbGoodsID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(16, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(812, 319);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "信息";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(563, 286);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(176, 25);
            this.txtRemarks.TabIndex = 14;
            // 
            // txtHandler
            // 
            this.txtHandler.Location = new System.Drawing.Point(159, 286);
            this.txtHandler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtHandler.Name = "txtHandler";
            this.txtHandler.Size = new System.Drawing.Size(176, 25);
            this.txtHandler.TabIndex = 13;
            // 
            // txtConsignee
            // 
            this.txtConsignee.Location = new System.Drawing.Point(563, 246);
            this.txtConsignee.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtConsignee.Name = "txtConsignee";
            this.txtConsignee.Size = new System.Drawing.Size(176, 25);
            this.txtConsignee.TabIndex = 12;
            // 
            // txtPickUpUnit
            // 
            this.txtPickUpUnit.Location = new System.Drawing.Point(159, 246);
            this.txtPickUpUnit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPickUpUnit.Name = "txtPickUpUnit";
            this.txtPickUpUnit.Size = new System.Drawing.Size(176, 25);
            this.txtPickUpUnit.TabIndex = 11;
            // 
            // txtOutStoreTotal
            // 
            this.txtOutStoreTotal.Location = new System.Drawing.Point(159, 201);
            this.txtOutStoreTotal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOutStoreTotal.Name = "txtOutStoreTotal";
            this.txtOutStoreTotal.Size = new System.Drawing.Size(176, 25);
            this.txtOutStoreTotal.TabIndex = 10;
            // 
            // txtOutStoreQuantity
            // 
            this.txtOutStoreQuantity.Location = new System.Drawing.Point(563, 158);
            this.txtOutStoreQuantity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOutStoreQuantity.Name = "txtOutStoreQuantity";
            this.txtOutStoreQuantity.Size = new System.Drawing.Size(176, 25);
            this.txtOutStoreQuantity.TabIndex = 9;
            this.txtOutStoreQuantity.TextChanged += new System.EventHandler(this.txtOutStoreQuantity_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(468, 292);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(84, 15);
            this.label14.TabIndex = 4;
            this.label14.Text = "备    注：";
            // 
            // txtOutStoreMonovalent
            // 
            this.txtOutStoreMonovalent.Location = new System.Drawing.Point(159, 152);
            this.txtOutStoreMonovalent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtOutStoreMonovalent.Name = "txtOutStoreMonovalent";
            this.txtOutStoreMonovalent.Size = new System.Drawing.Size(176, 25);
            this.txtOutStoreMonovalent.TabIndex = 8;
            this.txtOutStoreMonovalent.TextChanged += new System.EventHandler(this.txtOutStoreMonovalent_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(64, 292);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 15);
            this.label13.TabIndex = 4;
            this.label13.Text = "经 手 人：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(468, 252);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 15);
            this.label12.TabIndex = 4;
            this.label12.Text = "提 货 人：";
            // 
            // cmbSupplierName
            // 
            this.cmbSupplierName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSupplierName.FormattingEnabled = true;
            this.cmbSupplierName.Location = new System.Drawing.Point(563, 68);
            this.cmbSupplierName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSupplierName.Name = "cmbSupplierName";
            this.cmbSupplierName.Size = new System.Drawing.Size(176, 23);
            this.cmbSupplierName.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(64, 252);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 4;
            this.label11.Text = "提货单位：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(67, 206);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "总 金 额：";
            // 
            // cmbOutStoreType
            // 
            this.cmbOutStoreType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOutStoreType.FormattingEnabled = true;
            this.cmbOutStoreType.Location = new System.Drawing.Point(563, 202);
            this.cmbOutStoreType.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbOutStoreType.Name = "cmbOutStoreType";
            this.cmbOutStoreType.Size = new System.Drawing.Size(176, 23);
            this.cmbOutStoreType.TabIndex = 7;
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Location = new System.Drawing.Point(563, 114);
            this.cmbUnits.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(176, 23);
            this.cmbUnits.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(468, 162);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 4;
            this.label9.Text = "出库数量：";
            // 
            // cmbSpe
            // 
            this.cmbSpe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpe.FormattingEnabled = true;
            this.cmbSpe.Location = new System.Drawing.Point(159, 110);
            this.cmbSpe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSpe.Name = "cmbSpe";
            this.cmbSpe.Size = new System.Drawing.Size(176, 23);
            this.cmbSpe.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 162);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 4;
            this.label8.Text = "出库单价：";
            // 
            // cmbStoreName
            // 
            this.cmbStoreName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStoreName.FormattingEnabled = true;
            this.cmbStoreName.Location = new System.Drawing.Point(159, 68);
            this.cmbStoreName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbStoreName.Name = "cmbStoreName";
            this.cmbStoreName.Size = new System.Drawing.Size(176, 23);
            this.cmbStoreName.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(468, 206);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "出库方式：";
            // 
            // cmbGoodsName
            // 
            this.cmbGoodsName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsName.FormattingEnabled = true;
            this.cmbGoodsName.Location = new System.Drawing.Point(563, 25);
            this.cmbGoodsName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbGoodsName.Name = "cmbGoodsName";
            this.cmbGoodsName.Size = new System.Drawing.Size(176, 23);
            this.cmbGoodsName.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(475, 119);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "计量单位：";
            // 
            // cmbGoodsID
            // 
            this.cmbGoodsID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsID.FormattingEnabled = true;
            this.cmbGoodsID.Location = new System.Drawing.Point(159, 25);
            this.cmbGoodsID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbGoodsID.MaxDropDownItems = 10;
            this.cmbGoodsID.Name = "cmbGoodsID";
            this.cmbGoodsID.Size = new System.Drawing.Size(176, 23);
            this.cmbGoodsID.TabIndex = 1;
            this.cmbGoodsID.SelectionChangeCommitted += new System.EventHandler(this.cmbGoodsNum_SelectionChangeCommitted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(64, 115);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "规    格：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(475, 71);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "供 应 商：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "仓库名称：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(475, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "货物名称：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "货物编号：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnPrint);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.btnRevise);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnOutStorage);
            this.groupBox2.Location = new System.Drawing.Point(865, 12);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(189, 319);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "操作";
            // 
            // btnPrint
            // 
            this.btnPrint.ContextMenuStrip = this.cmsClearPrintList;
            this.btnPrint.Location = new System.Drawing.Point(45, 208);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 29);
            this.btnPrint.TabIndex = 19;
            this.btnPrint.Text = "打印";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnPrint_MouseClick);
            // 
            // cmsClearPrintList
            // 
            this.cmsClearPrintList.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsClearPrintList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空待打印ToolStripMenuItem});
            this.cmsClearPrintList.Name = "cmsClearPrintList";
            this.cmsClearPrintList.Size = new System.Drawing.Size(154, 28);
            // 
            // 清空待打印ToolStripMenuItem
            // 
            this.清空待打印ToolStripMenuItem.Name = "清空待打印ToolStripMenuItem";
            this.清空待打印ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.清空待打印ToolStripMenuItem.Text = "清空待打印";
            this.清空待打印ToolStripMenuItem.Click += new System.EventHandler(this.清空待打印ToolStripMenuItem_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(45, 266);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 29);
            this.btnExit.TabIndex = 18;
            this.btnExit.Text = "退出";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRevise
            // 
            this.btnRevise.Location = new System.Drawing.Point(45, 98);
            this.btnRevise.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(100, 29);
            this.btnRevise.TabIndex = 16;
            this.btnRevise.Text = "修改";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(45, 154);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 17;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnOutStorage
            // 
            this.btnOutStorage.Location = new System.Drawing.Point(45, 40);
            this.btnOutStorage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnOutStorage.Name = "btnOutStorage";
            this.btnOutStorage.Size = new System.Drawing.Size(100, 29);
            this.btnOutStorage.TabIndex = 15;
            this.btnOutStorage.Text = "出库";
            this.btnOutStorage.UseVisualStyleBackColor = true;
            this.btnOutStorage.Click += new System.EventHandler(this.btnOutStorage_Click);
            // 
            // dgvOutStoreInfo
            // 
            this.dgvOutStoreInfo.AllowUserToAddRows = false;
            this.dgvOutStoreInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvOutStoreInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOutStoreInfo.ContextMenuStrip = this.cmsAddPrint;
            this.dgvOutStoreInfo.Location = new System.Drawing.Point(16, 358);
            this.dgvOutStoreInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvOutStoreInfo.Name = "dgvOutStoreInfo";
            this.dgvOutStoreInfo.ReadOnly = true;
            this.dgvOutStoreInfo.RowHeadersWidth = 51;
            this.dgvOutStoreInfo.RowTemplate.Height = 23;
            this.dgvOutStoreInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOutStoreInfo.Size = new System.Drawing.Size(1037, 198);
            this.dgvOutStoreInfo.TabIndex = 19;
            this.dgvOutStoreInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOutStoreInfo_CellClick);
            this.dgvOutStoreInfo.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvOutStoreInfo_CellMouseClick);
            // 
            // cmsAddPrint
            // 
            this.cmsAddPrint.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsAddPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加到待打印ToolStripMenuItem});
            this.cmsAddPrint.Name = "cmsAddPrint";
            this.cmsAddPrint.Size = new System.Drawing.Size(169, 28);
            // 
            // 添加到待打印ToolStripMenuItem
            // 
            this.添加到待打印ToolStripMenuItem.Name = "添加到待打印ToolStripMenuItem";
            this.添加到待打印ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.添加到待打印ToolStripMenuItem.Text = "添加到待打印";
            this.添加到待打印ToolStripMenuItem.Click += new System.EventHandler(this.添加到待打印ToolStripMenuItem_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 339);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 25;
            this.label15.Text = "出库记录";
            // 
            // FrmOutStoreManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 564);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvOutStoreInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOutStoreManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "出库管理";
            this.Load += new System.EventHandler(this.FrmOutStoreManager_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.cmsClearPrintList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOutStoreInfo)).EndInit();
            this.cmsAddPrint.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtHandler;
        private System.Windows.Forms.TextBox txtConsignee;
        private System.Windows.Forms.TextBox txtPickUpUnit;
        private System.Windows.Forms.TextBox txtOutStoreTotal;
        private System.Windows.Forms.TextBox txtOutStoreQuantity;
        private System.Windows.Forms.TextBox txtOutStoreMonovalent;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbSupplierName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbOutStoreType;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbSpe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbStoreName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbGoodsName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbGoodsID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnOutStorage;
        private System.Windows.Forms.DataGridView dgvOutStoreInfo;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ContextMenuStrip cmsAddPrint;
        private System.Windows.Forms.ToolStripMenuItem 添加到待打印ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsClearPrintList;
        private System.Windows.Forms.ToolStripMenuItem 清空待打印ToolStripMenuItem;
    }
}