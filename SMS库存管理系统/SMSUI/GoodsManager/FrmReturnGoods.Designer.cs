﻿namespace SMSUI.GoodsManager
{
    partial class FrmReturnGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label19 = new System.Windows.Forms.Label();
            this.dgvReturnGoodsInfo = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnRevise = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnReturnGoods = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtHander = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtReturnGoodsMan = new System.Windows.Forms.TextBox();
            this.txtReturnGoodsQuanty = new System.Windows.Forms.TextBox();
            this.txtReturnGoodsTime = new System.Windows.Forms.TextBox();
            this.cmbReturnGoodsUnit = new System.Windows.Forms.ComboBox();
            this.cmbGoodsSpec = new System.Windows.Forms.ComboBox();
            this.cmbGoodsUnits = new System.Windows.Forms.ComboBox();
            this.cmbStoreName = new System.Windows.Forms.ComboBox();
            this.cmbGoodsName = new System.Windows.Forms.ComboBox();
            this.cmbGoodsID = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cmsAddPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加到待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsClearPrintList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.清空待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReturnGoodsInfo)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.cmsAddPrint.SuspendLayout();
            this.cmsClearPrintList.SuspendLayout();
            this.SuspendLayout();
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 324);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 15);
            this.label19.TabIndex = 26;
            this.label19.Text = "还货信息：";
            // 
            // dgvReturnGoodsInfo
            // 
            this.dgvReturnGoodsInfo.AllowUserToAddRows = false;
            this.dgvReturnGoodsInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvReturnGoodsInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReturnGoodsInfo.ContextMenuStrip = this.cmsAddPrint;
            this.dgvReturnGoodsInfo.Location = new System.Drawing.Point(11, 342);
            this.dgvReturnGoodsInfo.Margin = new System.Windows.Forms.Padding(4);
            this.dgvReturnGoodsInfo.MultiSelect = false;
            this.dgvReturnGoodsInfo.Name = "dgvReturnGoodsInfo";
            this.dgvReturnGoodsInfo.ReadOnly = true;
            this.dgvReturnGoodsInfo.RowHeadersWidth = 51;
            this.dgvReturnGoodsInfo.RowTemplate.Height = 23;
            this.dgvReturnGoodsInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReturnGoodsInfo.Size = new System.Drawing.Size(1044, 215);
            this.dgvReturnGoodsInfo.TabIndex = 18;
            this.dgvReturnGoodsInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReturnGoodsInfo_CellClick);
            this.dgvReturnGoodsInfo.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvReturnGoodsInfo_CellMouseClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPrint);
            this.groupBox3.Controls.Add(this.btnExit);
            this.groupBox3.Controls.Add(this.btnRevise);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Controls.Add(this.btnReturnGoods);
            this.groupBox3.Location = new System.Drawing.Point(851, 12);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(204, 308);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "操作";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(52, 260);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 29);
            this.btnExit.TabIndex = 17;
            this.btnExit.Text = "退出";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnRevise
            // 
            this.btnRevise.Location = new System.Drawing.Point(52, 96);
            this.btnRevise.Margin = new System.Windows.Forms.Padding(4);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(100, 29);
            this.btnRevise.TabIndex = 15;
            this.btnRevise.Text = "修改";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(52, 152);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnReturnGoods
            // 
            this.btnReturnGoods.Location = new System.Drawing.Point(52, 38);
            this.btnReturnGoods.Margin = new System.Windows.Forms.Padding(4);
            this.btnReturnGoods.Name = "btnReturnGoods";
            this.btnReturnGoods.Size = new System.Drawing.Size(100, 29);
            this.btnReturnGoods.TabIndex = 14;
            this.btnReturnGoods.Text = "还货";
            this.btnReturnGoods.UseVisualStyleBackColor = true;
            this.btnReturnGoods.Click += new System.EventHandler(this.btnReturnGoods_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtRemarks);
            this.groupBox4.Controls.Add(this.txtHander);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtPhone);
            this.groupBox4.Controls.Add(this.txtReturnGoodsMan);
            this.groupBox4.Controls.Add(this.txtReturnGoodsQuanty);
            this.groupBox4.Controls.Add(this.txtReturnGoodsTime);
            this.groupBox4.Controls.Add(this.cmbReturnGoodsUnit);
            this.groupBox4.Controls.Add(this.cmbGoodsSpec);
            this.groupBox4.Controls.Add(this.cmbGoodsUnits);
            this.groupBox4.Controls.Add(this.cmbStoreName);
            this.groupBox4.Controls.Add(this.cmbGoodsName);
            this.groupBox4.Controls.Add(this.cmbGoodsID);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Location = new System.Drawing.Point(11, 12);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(801, 308);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "信息";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(523, 260);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(177, 25);
            this.txtRemarks.TabIndex = 13;
            // 
            // txtHander
            // 
            this.txtHander.Location = new System.Drawing.Point(153, 260);
            this.txtHander.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHander.Name = "txtHander";
            this.txtHander.Size = new System.Drawing.Size(177, 25);
            this.txtHander.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(449, 266);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 20;
            this.label5.Text = "备  注";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(79, 266);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "经 手 人";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(153, 75);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(177, 25);
            this.txtPhone.TabIndex = 3;
            // 
            // txtReturnGoodsMan
            // 
            this.txtReturnGoodsMan.Location = new System.Drawing.Point(523, 24);
            this.txtReturnGoodsMan.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtReturnGoodsMan.Name = "txtReturnGoodsMan";
            this.txtReturnGoodsMan.Size = new System.Drawing.Size(177, 25);
            this.txtReturnGoodsMan.TabIndex = 2;
            // 
            // txtReturnGoodsQuanty
            // 
            this.txtReturnGoodsQuanty.Location = new System.Drawing.Point(153, 216);
            this.txtReturnGoodsQuanty.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtReturnGoodsQuanty.Name = "txtReturnGoodsQuanty";
            this.txtReturnGoodsQuanty.Size = new System.Drawing.Size(177, 25);
            this.txtReturnGoodsQuanty.TabIndex = 9;
            // 
            // txtReturnGoodsTime
            // 
            this.txtReturnGoodsTime.Location = new System.Drawing.Point(523, 216);
            this.txtReturnGoodsTime.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtReturnGoodsTime.Name = "txtReturnGoodsTime";
            this.txtReturnGoodsTime.ReadOnly = true;
            this.txtReturnGoodsTime.Size = new System.Drawing.Size(177, 25);
            this.txtReturnGoodsTime.TabIndex = 11;
            // 
            // cmbReturnGoodsUnit
            // 
            this.cmbReturnGoodsUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbReturnGoodsUnit.FormattingEnabled = true;
            this.cmbReturnGoodsUnit.Location = new System.Drawing.Point(153, 30);
            this.cmbReturnGoodsUnit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbReturnGoodsUnit.Name = "cmbReturnGoodsUnit";
            this.cmbReturnGoodsUnit.Size = new System.Drawing.Size(177, 23);
            this.cmbReturnGoodsUnit.TabIndex = 1;
            // 
            // cmbGoodsSpec
            // 
            this.cmbGoodsSpec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsSpec.FormattingEnabled = true;
            this.cmbGoodsSpec.Location = new System.Drawing.Point(523, 121);
            this.cmbGoodsSpec.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbGoodsSpec.Name = "cmbGoodsSpec";
            this.cmbGoodsSpec.Size = new System.Drawing.Size(177, 23);
            this.cmbGoodsSpec.TabIndex = 6;
            // 
            // cmbGoodsUnits
            // 
            this.cmbGoodsUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsUnits.FormattingEnabled = true;
            this.cmbGoodsUnits.Location = new System.Drawing.Point(153, 169);
            this.cmbGoodsUnits.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbGoodsUnits.Name = "cmbGoodsUnits";
            this.cmbGoodsUnits.Size = new System.Drawing.Size(177, 23);
            this.cmbGoodsUnits.TabIndex = 7;
            // 
            // cmbStoreName
            // 
            this.cmbStoreName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStoreName.FormattingEnabled = true;
            this.cmbStoreName.Location = new System.Drawing.Point(523, 169);
            this.cmbStoreName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbStoreName.Name = "cmbStoreName";
            this.cmbStoreName.Size = new System.Drawing.Size(177, 23);
            this.cmbStoreName.TabIndex = 8;
            // 
            // cmbGoodsName
            // 
            this.cmbGoodsName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsName.FormattingEnabled = true;
            this.cmbGoodsName.Location = new System.Drawing.Point(153, 122);
            this.cmbGoodsName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbGoodsName.Name = "cmbGoodsName";
            this.cmbGoodsName.Size = new System.Drawing.Size(177, 23);
            this.cmbGoodsName.TabIndex = 5;
            // 
            // cmbGoodsID
            // 
            this.cmbGoodsID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsID.FormattingEnabled = true;
            this.cmbGoodsID.Location = new System.Drawing.Point(523, 75);
            this.cmbGoodsID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbGoodsID.Name = "cmbGoodsID";
            this.cmbGoodsID.Size = new System.Drawing.Size(177, 23);
            this.cmbGoodsID.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(448, 80);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "货物编号";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "联系电话";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(77, 35);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 12;
            this.label12.Text = "还货单位";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 171);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "计量单位";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(448, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 15);
            this.label13.TabIndex = 10;
            this.label13.Text = "还 货 人";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(80, 221);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "还货数量";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(448, 172);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 8;
            this.label14.Text = "仓库名称";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(449, 221);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(67, 15);
            this.label15.TabIndex = 6;
            this.label15.Text = "还货时间";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(448, 124);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 15);
            this.label16.TabIndex = 4;
            this.label16.Text = "货物规格";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(77, 125);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 2;
            this.label17.Text = "货物名称";
            // 
            // btnPrint
            // 
            this.btnPrint.ContextMenuStrip = this.cmsClearPrintList;
            this.btnPrint.Location = new System.Drawing.Point(52, 206);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 29);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.Text = "打印";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnPrint_MouseClick);
            // 
            // cmsAddPrint
            // 
            this.cmsAddPrint.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsAddPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加到待打印ToolStripMenuItem});
            this.cmsAddPrint.Name = "cmsAddPrint";
            this.cmsAddPrint.Size = new System.Drawing.Size(169, 28);
            // 
            // 添加到待打印ToolStripMenuItem
            // 
            this.添加到待打印ToolStripMenuItem.Name = "添加到待打印ToolStripMenuItem";
            this.添加到待打印ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.添加到待打印ToolStripMenuItem.Text = "添加到待打印";
            this.添加到待打印ToolStripMenuItem.Click += new System.EventHandler(this.添加到待打印ToolStripMenuItem_Click);
            // 
            // cmsClearPrintList
            // 
            this.cmsClearPrintList.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsClearPrintList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空待打印ToolStripMenuItem});
            this.cmsClearPrintList.Name = "cmsClearPrintList";
            this.cmsClearPrintList.Size = new System.Drawing.Size(211, 56);
            // 
            // 清空待打印ToolStripMenuItem
            // 
            this.清空待打印ToolStripMenuItem.Name = "清空待打印ToolStripMenuItem";
            this.清空待打印ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.清空待打印ToolStripMenuItem.Text = "清空待打印";
            this.清空待打印ToolStripMenuItem.Click += new System.EventHandler(this.清空待打印ToolStripMenuItem_Click);
            // 
            // FrmReturnGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 564);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.dgvReturnGoodsInfo);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmReturnGoods";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "还货管理";
            this.Load += new System.EventHandler(this.FrmReturnGoods_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReturnGoodsInfo)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.cmsAddPrint.ResumeLayout(false);
            this.cmsClearPrintList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView dgvReturnGoodsInfo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtReturnGoodsMan;
        private System.Windows.Forms.ComboBox cmbGoodsID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnReturnGoods;
        private System.Windows.Forms.TextBox txtReturnGoodsTime;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TextBox txtHander;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtReturnGoodsQuanty;
        private System.Windows.Forms.ComboBox cmbReturnGoodsUnit;
        private System.Windows.Forms.ComboBox cmbGoodsSpec;
        private System.Windows.Forms.ComboBox cmbGoodsUnits;
        private System.Windows.Forms.ComboBox cmbStoreName;
        private System.Windows.Forms.ComboBox cmbGoodsName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ContextMenuStrip cmsAddPrint;
        private System.Windows.Forms.ToolStripMenuItem 添加到待打印ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsClearPrintList;
        private System.Windows.Forms.ToolStripMenuItem 清空待打印ToolStripMenuItem;
    }
}