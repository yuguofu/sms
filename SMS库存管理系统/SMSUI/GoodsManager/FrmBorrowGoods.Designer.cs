﻿namespace SMSUI.GoodsManager
{
    partial class FrmBorrowGoods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvBorrowGoodsInfo = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbUnits = new System.Windows.Forms.ComboBox();
            this.cmbSpe = new System.Windows.Forms.ComboBox();
            this.cmbStoreName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbGoodsName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbGoodsNum = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtHandler = new System.Windows.Forms.TextBox();
            this.txtBorrowGoodsQuantity = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBorrower = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBorrowGoodsUnits = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRevise = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnBorrowGoods = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cmsAddPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsClearPrintList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.添加到待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.清空待打印ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBorrowGoodsInfo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.cmsAddPrint.SuspendLayout();
            this.cmsClearPrintList.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvBorrowGoodsInfo
            // 
            this.dgvBorrowGoodsInfo.AllowUserToAddRows = false;
            this.dgvBorrowGoodsInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvBorrowGoodsInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBorrowGoodsInfo.ContextMenuStrip = this.cmsAddPrint;
            this.dgvBorrowGoodsInfo.Location = new System.Drawing.Point(13, 332);
            this.dgvBorrowGoodsInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBorrowGoodsInfo.Name = "dgvBorrowGoodsInfo";
            this.dgvBorrowGoodsInfo.ReadOnly = true;
            this.dgvBorrowGoodsInfo.RowHeadersWidth = 51;
            this.dgvBorrowGoodsInfo.RowTemplate.Height = 23;
            this.dgvBorrowGoodsInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBorrowGoodsInfo.Size = new System.Drawing.Size(1035, 218);
            this.dgvBorrowGoodsInfo.TabIndex = 14;
            this.dgvBorrowGoodsInfo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBorrowGoodsInfo_CellClick);
            this.dgvBorrowGoodsInfo.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvBorrowGoodsInfo_CellMouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cmbUnits);
            this.groupBox1.Controls.Add(this.cmbSpe);
            this.groupBox1.Controls.Add(this.cmbStoreName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cmbGoodsName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cmbGoodsNum);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtRemarks);
            this.groupBox1.Controls.Add(this.txtHandler);
            this.groupBox1.Controls.Add(this.txtBorrowGoodsQuantity);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBorrower);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtBorrowGoodsUnits);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(819, 294);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "信息";
            // 
            // cmbUnits
            // 
            this.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnits.FormattingEnabled = true;
            this.cmbUnits.Location = new System.Drawing.Point(555, 168);
            this.cmbUnits.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUnits.Name = "cmbUnits";
            this.cmbUnits.Size = new System.Drawing.Size(176, 23);
            this.cmbUnits.TabIndex = 8;
            // 
            // cmbSpe
            // 
            this.cmbSpe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpe.FormattingEnabled = true;
            this.cmbSpe.Location = new System.Drawing.Point(144, 168);
            this.cmbSpe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSpe.Name = "cmbSpe";
            this.cmbSpe.Size = new System.Drawing.Size(176, 23);
            this.cmbSpe.TabIndex = 7;
            // 
            // cmbStoreName
            // 
            this.cmbStoreName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStoreName.FormattingEnabled = true;
            this.cmbStoreName.Location = new System.Drawing.Point(555, 119);
            this.cmbStoreName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbStoreName.Name = "cmbStoreName";
            this.cmbStoreName.Size = new System.Drawing.Size(176, 23);
            this.cmbStoreName.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(460, 172);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 8;
            this.label8.Text = "计量单位：";
            // 
            // cmbGoodsName
            // 
            this.cmbGoodsName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsName.FormattingEnabled = true;
            this.cmbGoodsName.Location = new System.Drawing.Point(144, 124);
            this.cmbGoodsName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbGoodsName.Name = "cmbGoodsName";
            this.cmbGoodsName.Size = new System.Drawing.Size(176, 23);
            this.cmbGoodsName.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(49, 172);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 15);
            this.label7.TabIndex = 8;
            this.label7.Text = "货物规格：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(460, 122);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 8;
            this.label6.Text = "所属仓库：";
            // 
            // cmbGoodsNum
            // 
            this.cmbGoodsNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoodsNum.FormattingEnabled = true;
            this.cmbGoodsNum.Location = new System.Drawing.Point(555, 75);
            this.cmbGoodsNum.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbGoodsNum.Name = "cmbGoodsNum";
            this.cmbGoodsNum.Size = new System.Drawing.Size(176, 23);
            this.cmbGoodsNum.TabIndex = 4;
            this.cmbGoodsNum.SelectionChangeCommitted += new System.EventHandler(this.cmbGoodsNum_SelectionChangeCommitted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 128);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "货物名称：";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(144, 256);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(176, 25);
            this.txtRemarks.TabIndex = 10;
            // 
            // txtHandler
            // 
            this.txtHandler.Location = new System.Drawing.Point(555, 212);
            this.txtHandler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtHandler.Name = "txtHandler";
            this.txtHandler.Size = new System.Drawing.Size(176, 25);
            this.txtHandler.TabIndex = 10;
            // 
            // txtBorrowGoodsQuantity
            // 
            this.txtBorrowGoodsQuantity.Location = new System.Drawing.Point(144, 212);
            this.txtBorrowGoodsQuantity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBorrowGoodsQuantity.Name = "txtBorrowGoodsQuantity";
            this.txtBorrowGoodsQuantity.Size = new System.Drawing.Size(176, 25);
            this.txtBorrowGoodsQuantity.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(49, 262);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 15);
            this.label12.TabIndex = 8;
            this.label12.Text = "备  注：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(460, 219);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 8;
            this.label10.Text = "经 手 人：";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(144, 75);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(176, 25);
            this.txtPhone.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(49, 218);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "借货数量：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(460, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "货物编号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "联系电话：";
            // 
            // txtBorrower
            // 
            this.txtBorrower.Location = new System.Drawing.Point(555, 30);
            this.txtBorrower.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBorrower.Name = "txtBorrower";
            this.txtBorrower.Size = new System.Drawing.Size(176, 25);
            this.txtBorrower.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(460, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "借 货 人：";
            // 
            // txtBorrowGoodsUnits
            // 
            this.txtBorrowGoodsUnits.Location = new System.Drawing.Point(144, 30);
            this.txtBorrowGoodsUnits.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBorrowGoodsUnits.Name = "txtBorrowGoodsUnits";
            this.txtBorrowGoodsUnits.Size = new System.Drawing.Size(176, 25);
            this.txtBorrowGoodsUnits.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(49, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "借货单位：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.btnPrint);
            this.groupBox2.Controls.Add(this.btnRevise);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnBorrowGoods);
            this.groupBox2.Location = new System.Drawing.Point(861, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(189, 294);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "操作";
            // 
            // btnRevise
            // 
            this.btnRevise.Location = new System.Drawing.Point(45, 84);
            this.btnRevise.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(100, 29);
            this.btnRevise.TabIndex = 12;
            this.btnRevise.Text = "修改";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(45, 137);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 12;
            this.btnDelete.Text = "删除";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnBorrowGoods
            // 
            this.btnBorrowGoods.Location = new System.Drawing.Point(45, 33);
            this.btnBorrowGoods.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBorrowGoods.Name = "btnBorrowGoods";
            this.btnBorrowGoods.Size = new System.Drawing.Size(100, 29);
            this.btnBorrowGoods.TabIndex = 11;
            this.btnBorrowGoods.Text = "借货";
            this.btnBorrowGoods.UseVisualStyleBackColor = true;
            this.btnBorrowGoods.Click += new System.EventHandler(this.btnBorrowGoods_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 314);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 8;
            this.label11.Text = "借货记录：";
            // 
            // btnPrint
            // 
            this.btnPrint.ContextMenuStrip = this.cmsClearPrintList;
            this.btnPrint.Location = new System.Drawing.Point(45, 193);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 29);
            this.btnPrint.TabIndex = 13;
            this.btnPrint.Text = "打印";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnPrint_MouseClick);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(45, 248);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 29);
            this.btnExit.TabIndex = 14;
            this.btnExit.Text = "退出";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cmsAddPrint
            // 
            this.cmsAddPrint.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsAddPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加到待打印ToolStripMenuItem});
            this.cmsAddPrint.Name = "cmsAddPrint";
            this.cmsAddPrint.Size = new System.Drawing.Size(169, 28);
            // 
            // cmsClearPrintList
            // 
            this.cmsClearPrintList.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsClearPrintList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空待打印ToolStripMenuItem});
            this.cmsClearPrintList.Name = "cmsClearPrintList";
            this.cmsClearPrintList.Size = new System.Drawing.Size(154, 28);
            // 
            // 添加到待打印ToolStripMenuItem
            // 
            this.添加到待打印ToolStripMenuItem.Name = "添加到待打印ToolStripMenuItem";
            this.添加到待打印ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.添加到待打印ToolStripMenuItem.Text = "添加到待打印";
            this.添加到待打印ToolStripMenuItem.Click += new System.EventHandler(this.添加到待打印ToolStripMenuItem_Click);
            // 
            // 清空待打印ToolStripMenuItem
            // 
            this.清空待打印ToolStripMenuItem.Name = "清空待打印ToolStripMenuItem";
            this.清空待打印ToolStripMenuItem.Size = new System.Drawing.Size(210, 24);
            this.清空待打印ToolStripMenuItem.Text = "清空待打印";
            this.清空待打印ToolStripMenuItem.Click += new System.EventHandler(this.清空待打印ToolStripMenuItem_Click);
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("宋体", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(461, 262);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(307, 19);
            this.label13.TabIndex = 13;
            this.label13.Text = "提示：右键列表添加待打印，右键打印按钮清空待打印。";
            // 
            // FrmBorrowGoods
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 564);
            this.Controls.Add(this.dgvBorrowGoodsInfo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label11);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmBorrowGoods";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "借货管理";
            this.Load += new System.EventHandler(this.FrmBorrowGoods_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBorrowGoodsInfo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.cmsAddPrint.ResumeLayout(false);
            this.cmsClearPrintList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvBorrowGoodsInfo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbSpe;
        private System.Windows.Forms.ComboBox cmbStoreName;
        private System.Windows.Forms.ComboBox cmbGoodsName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbGoodsNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBorrower;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBorrowGoodsUnits;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbUnits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHandler;
        private System.Windows.Forms.TextBox txtBorrowGoodsQuantity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnBorrowGoods;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ContextMenuStrip cmsAddPrint;
        private System.Windows.Forms.ToolStripMenuItem 添加到待打印ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsClearPrintList;
        private System.Windows.Forms.ToolStripMenuItem 清空待打印ToolStripMenuItem;
        private System.Windows.Forms.Label label13;
    }
}