﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SMSUI.QueryStatistics
{
    public partial class FrmBorrowGoodsQuery : Form
    {
        private BLL.GoodsManager.BorrowGoodsManager borrowGoodsManager = new BLL.GoodsManager.BorrowGoodsManager();
        private BLL.QueryStatistics.BorrowGoodsQueryManager borrowGoodsQueryManager = new BLL.QueryStatistics.BorrowGoodsQueryManager();
        public FrmBorrowGoodsQuery()
        {
            InitializeComponent();
        }

        private void FrmBorrowGoodsQuery_Load(object sender, EventArgs e)
        {
            DataSet ds = borrowGoodsManager.GetDataSetFromBorrowGoodsTable();
            dgvInStoreQueryResult.DataSource = ds.Tables[0];

        }

        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (cmbQueryStype.SelectedIndex != -1)
            {
                if (cmbQueryStype.Text == "查询全部")
                {
                    FrmBorrowGoodsQuery_Load(sender, e);
                }
                lblTip.Text = "";
                lblTip.Visible = false;
                dgvInStoreQueryResult.DataSource = null;
                try
                {
                    DataSet ds = borrowGoodsQueryManager.QueryBorrowGoodsInfoOfKeyWords(cmbQueryStype.Text, txtKeyWords.Text.Trim());
                    dgvInStoreQueryResult.DataSource = ds.Tables[0];
                    lblTip.Text = "查询完毕 共" + dgvInStoreQueryResult.RowCount + "条记录";
                    lblTip.Visible = true;
                }
                catch (Exception ex)
                {
                    lblTip.Text = "查询失败！";
                    lblTip.Visible = true;
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("请选择查询方式！");
            }
        }

        private void cmbQueryStype_SelectionChangeCommitted(object sender, EventArgs e)
        {
            lblTip.Visible = false;
        }

        private void txtKeyWords_Enter(object sender, EventArgs e)
        {
            lblTip.Visible = false;
        }

        private void txtKeyWords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnQuery_Click(sender, e);
            }
        }

        
    }
}
