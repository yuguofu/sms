﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMSModels;
using System.Data;

namespace DAL.QueryStatistics
{
    public class InStoreQueryService
    {

        public DataSet QueryInStoreInfoOfKeyWords(string queryType ,string queryKeyWords)
        { 
            string sql = "select * from (select 入库编号,入库表.货物编号,货物名称,入库表.仓库ID,仓库名称 as 所入仓库,入库表.供货商ID,供货商名称,规格,计量单位,入库单价,入库数量,入库总金额,入库时间,经手人,入库表.备注 from (select 货物编号,货物名称,供货商信息表.供货商名称,规格,计量单位,单价,货物档案表.备注 from 货物档案表,供货商信息表 where 货物档案表.供货商ID=供货商信息表.供货商ID) a,入库表,仓库信息表 where 入库表.货物编号=a.货物编号 and 入库表.仓库ID=仓库信息表.仓库ID) b";
            if (queryType == "货物编号")  //以货物ID查询
            {
                sql += string.Format(" where b.货物编号='{0}'", queryKeyWords);
            }
            if (queryType == "货物名称")    //以货物名称查询
            {
                sql += string.Format(" where b.货物名称 like '%{0}%'", queryKeyWords);
            }
            if (queryType == "仓库名称")    //以仓库名称查询
            {
                sql += string.Format(" where b.所入仓库='{0}'", queryKeyWords);
            }
            return SQLHelper.GetDataSet(sql,"queryResult");
        }
        
    }
}
