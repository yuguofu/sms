﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMSModels;
using System.Data;

namespace DAL.QueryStatistics
{
    public class ReturnGoodsQueryService
    {

        public DataSet QueryReturnGoodsInfoOfKeyWords(string queryType ,string queryKeyWords)
        { 
            string sql = "select * from (select 还货编号,还货单位,还货人,联系电话,还货表.货物编号,货物档案表.货物名称,规格,计量单位,还货表.仓库ID,仓库信息表.仓库名称,还货数量,还货时间,经手人,还货表.备注 from 还货表,货物档案表,仓库信息表 where 还货表.货物编号=货物档案表.货物编号 and 还货表.仓库ID=仓库信息表.仓库ID) b";
            if (queryType == "货物编号")  //以货物ID查询
            {
                sql += string.Format(" where b.货物编号='{0}'", queryKeyWords);
            }
            if (queryType == "货物名称")    //以货物名称查询
            {
                sql += string.Format(" where b.货物名称 like '%{0}%'", queryKeyWords);
            }
            if (queryType == "仓库名称")    //以仓库名称查询
            {
                sql += string.Format(" where b.仓库名称='{0}'", queryKeyWords);
            }
            return SQLHelper.GetDataSet(sql,"queryResult");
        }
        
    }
}
