﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SMSModels;
using System.Data.SQLite;

namespace DAL
{
   public  class StoreService
    {
        /// <summary>
        /// 添加仓库信息
        /// </summary>
        /// <param name="store">store对象</param>
        /// <returns>返回受影响行数</returns>
        public int AddStore(Store store)
        {
            //[1]封装SQL语句，添加仓库
            String sql = String.Format("insert into 仓库信息表 (仓库名称,负责人,负责人电话,地址,备注) values('{0}','{1}','{2}','{3}','{4}')", store.StoreName, store.PIC, store.Phone, store.Address, store.Remarks);
            String sqlStrIsExist = String.Format("select * from 仓库信息表 where 仓库名称='{0}'",store.StoreName);

            //[2]提交插入
            if (SQLHelper.GetReader(sqlStrIsExist).HasRows)
            {
                return -1;

            }
            else
            {
                return SQLHelper.Update(sql);
            }

        }

        /// <summary>
        /// 删除仓库
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int DeleteStore(Store store)
        {
            //[1]封装SQL语句，删除仓库
            String sql = String.Format("delete from 仓库信息表 where 仓库ID={0}",store.StoreID);

            //[2]提交插入
            return SQLHelper.Update(sql);

        }

        /// <summary>
        /// 修改仓库信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int UpdateStoreInfo(Store store)
        {
            //[1]封装SQL语句，修改仓库信息
            String sql = String.Format("update 仓库信息表 set 仓库名称='{0}', 负责人='{1}', 负责人电话='{2}', 地址='{3}', 备注='{4}' where 仓库ID={5}", store.StoreName, store.PIC, store.Phone, store.Address, store.Remarks, store.StoreID);

            //[2]提交修改
            return SQLHelper.Update(sql);
            

        }

        /// <summary>
        /// 获取仓库信息数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            String sql = "select * from 仓库信息表";
            String tbName = "storeInfo";
            return SQLHelper.GetDataSet(sql, tbName);
        }



        //public List<Store> GetStoreIDAndName()
        //{
        //    String sql = "select 仓库ID,仓库名称 from 仓库信息表";
        //    SQLiteDataReader reader = SQLHelper.GetReader(sql);
        //    List<Store> list = new List<Store>();
        //    while (reader.Read())
        //    {
        //        list.Add(new Store
        //        {
        //            StoreID = Convert.ToInt32(reader["仓库ID"]),
        //            StoreName = reader["仓库名称"].ToString()
        //        });
        //    }
        //    reader.Close();
        //    return list;

        //}
    }
}
