﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SQLite;
using SMSModels;

namespace DAL
{
    /// <summary>
    /// 用户数据访问类
    /// </summary>
    public class UsreService
    {
        #region 用户登录
        /// <summary>
        /// 用户登录（DAL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回登录用户对象，失败返回null对象</returns>
        public User UserLogin(User user)
        {
            //[1]封装SQL语句
            String sql = String.Format("select * from 用户信息表 where 用户名='{0}' and 密码='{1}'", user.UserName,user.Password);

            //[2]提交查询
            SQLiteDataReader reader = SQLHelper.GetReader(sql);

            //[3]判断登录是否正确，正确封装登录信息，错误返回null
            if (reader.Read())
            {
                user.UserID = Convert.ToInt32(reader["用户ID"]);
                user.UserName = reader["用户名"].ToString();
                user.UserType = reader["用户类型"].ToString();
                user.Power = (int)(reader["权限"]);
                user.Remarks = reader["备注"].ToString();
            }
            else
            {
                user = null;    //表示当前用户或密码不正确
            }
            reader.Close();
            return user;

        }
        #endregion


        /// <summary>
        /// 获取用户数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            String sql = "select * from 用户信息表";
            String tbName = "userInfo";
            return SQLHelper.GetDataSet(sql, tbName);
        }



        /// <summary>
        /// 添加用户（DAL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回受影响行数</returns>
        public int AddUser(User user)
        {
            //[1]封装SQL语句，添加用户
            String sql = String.Format("insert into 用户信息表 (用户名,密码,权限,用户类型,备注) values('{0}','{1}',{2},'{3}','{4}')", user.UserName, user.Password,user.Power, user.UserType,user.Remarks);
            String sqlStrIsExist = String.Format("select * from 用户信息表 where 用户名='{0}'", user.UserName);

            //[2]提交插入
            if (SQLHelper.GetReader(sqlStrIsExist).HasRows)
            {
                return -1;

            }
            else
            {
                return SQLHelper.Update(sql);
            }

        }


        /// <summary>
        /// 删除用户（DAL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回受影响行数</returns>
        public int DeleteUser(User user)
        {
            //[1]封装SQL语句，删除用户
            String sql = String.Format("delete from 用户信息表 where 用户ID='{0}'", user.UserID);

            //[2]提交删除
            return SQLHelper.Update(sql);

        }


        /// <summary>
        /// 修改用户信息（DAL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回受影响行数</returns>
        public int UpdateUserInfo(User user)
        {
            //[1]封装SQL语句，修改用户信息
            String sql = String.Format("update 用户信息表 set 用户名='{0}', 密码='{1}', 用户类型='{2}', 权限={3}, 备注='{4}' where 用户ID='{5}'", user.UserName,user.Password,user.UserType,user.Power,user.Remarks, user.UserID);

            //[2]提交修改
            return SQLHelper.Update(sql);
            

        }

    }
}
