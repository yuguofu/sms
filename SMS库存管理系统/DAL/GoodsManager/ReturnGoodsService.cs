﻿using SMSModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.GoodsManager
{
    public class ReturnGoodsService
    {
        /// <summary>
        /// 获取还货表dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSetFromReturnGoodsTable()
        {
            String sql = "select 还货编号,还货单位,还货人,联系电话,还货表.货物编号,货物档案表.货物名称,规格,计量单位,还货表.仓库ID,仓库信息表.仓库名称,还货数量,还货时间,经手人,还货表.备注 from 还货表,货物档案表,仓库信息表 where 还货表.货物编号=货物档案表.货物编号 and 还货表.仓库ID=仓库信息表.仓库ID";
            String tbName = "returnGoodsInfo";
            return SQLHelper.GetDataSet(sql, tbName);
        }

        /// <summary>
        /// 还货
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int ReturnGoods(ReturnGoods returnGoods)
        {
            String sql = String.Format("insert into 还货表 (还货编号,还货单位,还货人,联系电话,货物编号,仓库ID,还货数量,还货时间,经手人,备注) values('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}','{8}','{9}')", returnGoods.ReturnGoodsID, returnGoods.ReturnGoodsUnits, returnGoods.Returner, returnGoods.ReturnerPhone, returnGoods.GoodsID, returnGoods.StoreID, returnGoods.ReturnGoodsQuantity, returnGoods.ReturnGoodsTime, returnGoods.Handler, returnGoods.Remarks);
            return SQLHelper.Update(sql);

        }


        /// <summary>
        /// 还货时更新库存
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int UpdateStorageOnReturnGoods(ReturnGoods returnGoods)
        {
            //[1]封装SQL语句
            String sqlUpdateStorage = String.Format("UPDATE 库存表 SET 库存数量=库存表.库存数量+{0} WHERE 库存表.货物编号='{1}' and 仓库ID={2}", returnGoods.ReturnGoodsQuantity, returnGoods.GoodsID, returnGoods.StoreID);
            //提交  更新库存
            return SQLHelper.Update(sqlUpdateStorage);
        }

        /// <summary>
        /// 修改还货信息
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int ReviseReturnGoodsInfo(ReturnGoods returnGoods)
        {
            //[1]封装SQL语句，修改还货信息
            string sql = string.Format("update 还货表 set 还货单位='{0}',还货人='{1}',联系电话='{2}',货物编号='{3}',仓库ID={4},还货数量={5},还货时间='{6}',经手人='{7}',备注='{8}' where 还货编号='{9}'", returnGoods.ReturnGoodsUnits, returnGoods.Returner, returnGoods.ReturnerPhone, returnGoods.GoodsID, returnGoods.StoreID, returnGoods.ReturnGoodsQuantity, returnGoods.ReturnGoodsTime, returnGoods.Handler, returnGoods.Remarks, returnGoods.ReturnGoodsID);

            //[2]提交修改
            return SQLHelper.Update(sql);
        }


        /// <summary>
        /// 删除还货信息
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int DeleteReturnGoodsInfo(ReturnGoods returnGoods)
        {
            //[1]封装SQL语句，删除出库信息
            String sql = String.Format("delete from 还货表 where 还货编号='{0}'", returnGoods.ReturnGoodsID);

            //[2]提交删除
            return SQLHelper.Update(sql);

        }
    }
}
