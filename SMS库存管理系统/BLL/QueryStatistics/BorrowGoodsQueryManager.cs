﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.QueryStatistics
{
    public class BorrowGoodsQueryManager
    {
        private DAL.QueryStatistics.BorrowGoodsQueryService borrowGoodsQueryService = new DAL.QueryStatistics.BorrowGoodsQueryService();
        /// <summary>
        /// 获取借货表dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            return borrowGoodsQueryService.GetDataSet();
        }



        public List<BorrowGoods> GetBorrowGoodsInfoList()
        {
            return borrowGoodsQueryService.GetBorrowGoodsInfoList();
        }


        public DataSet QueryBorrowGoodsInfoOfKeyWords(string queryType, string queryKeyWords)
        {
            return borrowGoodsQueryService.QueryBorrowGoodsInfoOfKeyWords(queryType, queryKeyWords);
        }
    }
}
