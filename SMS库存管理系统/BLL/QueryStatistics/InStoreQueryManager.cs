﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL.QueryStatistics
{
    public class InStoreQueryManager
    {
        private DAL.QueryStatistics.InStoreQueryService inStoreQueryService = new DAL.QueryStatistics.InStoreQueryService();
        public DataSet QueryInStoreInfoOfKeyWords(string queryType, string queryKeyWords)
        {
            return inStoreQueryService.QueryInStoreInfoOfKeyWords(queryType, queryKeyWords);
        }
    }
}
