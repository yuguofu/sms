﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.QueryStatistics
{
    public class StorageManager
    {
        private DAL.QueryStatistics.StorageQueryService storageService = new DAL.QueryStatistics.StorageQueryService();

        /// <summary>
        /// 获取库存表dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            return storageService.GetDataSet();
        }

        /// <summary>
        /// 获取现有库存list借货
        /// </summary>
        /// <returns></returns>
        public List<Storage> GetCurrentStorageList()
        {
            return storageService.GetCurrentStorageList();
        }


        /// <summary>
        /// 选择货物编号时查询对应编号货物信息
        /// </summary>
        /// <param name="goodsID">编号编号</param>
        /// <returns></returns>
        public List<Storage> GetListOnSelectGoodsID(string goodsID)
        {
            return storageService.GetListOnSelectGoodsID(goodsID);
        }



    }
}
