﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL.QueryStatistics
{
    public class OutStoreQueryManager
    {
        private DAL.QueryStatistics.OutStoreQueryService OutStoreQueryService = new DAL.QueryStatistics.OutStoreQueryService();
        public DataSet QueryOutStoreInfoOfKeyWords(string queryType, string queryKeyWords)
        {
            return OutStoreQueryService.QueryOutStoreInfoOfKeyWords(queryType, queryKeyWords);
        }
    }
}
