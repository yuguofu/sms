﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL.QueryStatistics
{
    public class ReturnGoodsQueryManager
    {
        private DAL.QueryStatistics.ReturnGoodsQueryService returnGoodsQueryService = new DAL.QueryStatistics.ReturnGoodsQueryService();
        public DataSet QueryReturnGoodsInfoOfKeyWords(string queryType, string queryKeyWords)
        {
            return returnGoodsQueryService.QueryReturnGoodsInfoOfKeyWords(queryType, queryKeyWords);
        }
    }
}
