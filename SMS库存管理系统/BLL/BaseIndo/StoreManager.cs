﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.BaseIndo
{
    public class StoreManager
    {
        private StoreService storeService = new StoreService();

        /// <summary>
        /// 添加仓库
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int AddStore(Store store)
        {
            return storeService.AddStore(store);
        }

        /// <summary>
        /// 删除仓库
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int DeleteStore(Store store)
        {
            return storeService.DeleteStore(store);
        }

        /// <summary>
        /// 修改仓库信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int UpdateStoreInfo(Store store)
        {
            return storeService.UpdateStoreInfo(store);
        }

        /// <summary>
        /// 获取仓库信息数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            return storeService.GetDataSet();
        }


    }
}
