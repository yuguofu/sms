﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.BaseIndo
{
    public class GoodsFilesManager
    {
        private GoodsFilesService goodsFilesService = new GoodsFilesService();

        /// <summary>
        /// 添加货物档案
        /// </summary>
        /// <param name="goodsFiles"></param>
        /// <returns></returns>
        public int AddGoodsFile(GoodsFiles goodsFiles)
        {
            return goodsFilesService.AddGoodsFile(goodsFiles);
        }


        /// <summary>
        /// 删除货物档案
        /// </summary>
        /// <param name="goodsFiles"></param>
        /// <returns></returns>
        public int DeleteGoodsFiles(GoodsFiles goodsFiles)
        {
            return goodsFilesService.DeleteGoodsFiles(goodsFiles);
        }

        /// <summary>
        /// 修改货物档案
        /// </summary>
        /// <param name="goodsFiles"></param>
        /// <returns></returns>
        public int UpdateGoodsFilesInfo(GoodsFiles goodsFiles)
        {
            return goodsFilesService.UpdateGoodsFilesInfo(goodsFiles);
        }

        /// <summary>
        /// 获取货物档案信息数据集对象
        /// </summary>
        /// <returns>返回dataset对象</returns>
        public DataSet GetDataSet()
        {
            return goodsFilesService.GetDataSet();
        }


        /// <summary>
        /// 获取货物档案list集合
        /// </summary>
        /// <returns></returns>
        public List<GoodsFiles> GetGoodsInfo()
        {
            return goodsFilesService.GetGoodsInfo();
        }





        /// <summary>
        /// 获取货物信息，货物编号组合框选择触发方法
        /// </summary>
        /// <param name="goodsNum"></param>
        /// <returns></returns>
        public List<GoodsFiles> GetListOnSelectGoodsNum(String goodsNum)
        {
            return goodsFilesService.GetListOnSelectGoodsNum(goodsNum);

        }


        /// <summary>
        /// 货物编号是否存在
        /// </summary>
        /// <param name="goodsID"></param>
        /// <returns>存在返回true，否则返回false</returns>
        public bool GoodsIDIsExist(string goodsID)
        {
            return goodsFilesService.GoodsIDIsExist(goodsID);
        }
    }
}
