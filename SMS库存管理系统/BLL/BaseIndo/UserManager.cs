﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using SMSModels;

namespace BLL.BaseIndo
{
    //用户管理类(逻辑层)
    public class UserManager
    {
        private UsreService userService = new UsreService();


        /// <summary>
        /// 用户登录（BLL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回登录用户对象，失败返回null对象</returns>
        public User UserLogin(User user)
        {
            user = userService.UserLogin(user);
            return user;
        }

        /// <summary>
        /// 获取用户数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            return userService.GetDataSet();
        }



        /// <summary>
        /// 添加用户（BLL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回受影响行数</returns>
        public int AddUser(User user)
        {
            return userService.AddUser(user);
        }

        /// <summary>
        /// 删除用户（BLL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回受影响行数</returns>
        public int DeleteUser(User user)
        {
            return userService.DeleteUser(user);
        }

        /// <summary>
        /// 修改用户信息（BLL）
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>返回受影响行数</returns>
        public int UpdateUserInfo(User user)
        {
            return userService.UpdateUserInfo(user);
        }
    }
}
