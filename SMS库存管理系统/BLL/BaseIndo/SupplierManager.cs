﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using SMSModels;

namespace BLL.BaseIndo
{
    public class SupplierManager
    {
        private SupplierService supplierService = new SupplierService();

        /// <summary>
        /// 添加供货商
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int AddSupplier(Supplier supplier)
        {
            return supplierService.AddSupplier(supplier);
        }

        /// <summary>
        /// 删除供货商
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int DeleteSupplier(Supplier supplier)
        {
            return supplierService.DeleteSupplier(supplier);
        }

        /// <summary>
        /// 修改供货商信息
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public int UpdateStoreInfo(Supplier supplier)
        {
            return supplierService.UpdateSupplierInfo(supplier);
        }

        /// <summary>
        /// 获取供货商信息数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSet()
        {
            return supplierService.GetDataSet();
        }


        
    }
}
