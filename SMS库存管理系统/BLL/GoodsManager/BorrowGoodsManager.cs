﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.GoodsManager
{
    public  class BorrowGoodsManager
    {
        private DAL.GoodsManager.BorrowGoodsService borrowGoodsService = new DAL.GoodsManager.BorrowGoodsService();

        /// <summary>
        /// 获取 借货表 dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSetFromBorrowGoodsTable()
        {
            return borrowGoodsService.GetDataSetFromBorrowGoodsTable();
        }


        /// <summary>
        /// 借货前判断库存中货物是否充足
        /// </summary>
        /// <param name="borrowGoods">borrowGoods对象</param>
        /// <returns>充足返回true，否则返回false</returns>
        public bool IsEnoughOfBorrowGoods(BorrowGoods borrowGoods)
        {
            return borrowGoodsService.IsEnough(borrowGoods);
        }


        /// <summary>
        /// 借货（插入借货表记录）
        /// </summary>
        /// <param name="borrowGoods"></param>
        /// <returns></returns>
        public int BorrowGoods(BorrowGoods borrowGoods)
        {
            return borrowGoodsService.BorrowGoods(borrowGoods);
        }


        /// <summary>
        /// 借货时更新库存
        /// </summary>
        /// <param name="borrowGoods"></param>
        /// <returns></returns>
        public int UpdateStorageOnBorrowGoods(BorrowGoods borrowGoods)
        {
            return borrowGoodsService.UpdateStorageOnBorrowGoods(borrowGoods); //更新库存数量
        }


        /// <summary>
        /// 修改借货信息
        /// </summary>
        /// <param name="borrowGoods"></param>
        /// <returns></returns>
        public int ReviseBorrowGoodsInfo(BorrowGoods borrowGoods)
        {
            return borrowGoodsService.ReviseBorrowGoodsInfo(borrowGoods);
        }


        /// <summary>
        /// 删除借货信息
        /// </summary>
        /// <param name="borrowGoods"></param>
        /// <returns></returns>
        public int DeleteBorrowGoodsInfo(BorrowGoods borrowGoods)
        {
            return borrowGoodsService.DeleteBorrowGoodsInfo(borrowGoods);
        }
    }
}
