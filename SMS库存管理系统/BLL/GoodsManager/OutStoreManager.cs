﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.GoodsManager
{
    public  class OutStoreManager
    {
        private DAL.GoodsManager.OutStoreService outStoreService = new DAL.GoodsManager.OutStoreService();

        /// <summary>
        /// 获取出库表数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSetFromOutStoreTable()
        {
            return outStoreService.GetDataSetFromOutStoreTable();
        }


        /// <summary>
        /// 判断库存中货物是否充足
        /// </summary>
        /// <param name="outStore">goods对象</param>
        /// <returns>充足返回true，否则返回false</returns>
        public bool IsEnough(OutStore outStore)
        {
            return outStoreService.IsEnough(outStore);
        }


        /// <summary>
        /// 货物出库
        /// </summary>
        /// <param name="outStore"></param>
        /// <returns></returns>
        public int OutStore(OutStore outStore)
        {
            return outStoreService.OutStore(outStore);
        }


        /// <summary>
        /// 出库时更新库存数量
        /// </summary>
        /// <param name="outStore"></param>
        /// <returns></returns>
        public int UpdateStorageOnOutStore(OutStore outStore)
        {
            return outStoreService.UpdateStorageOnOutStore(outStore); //更新库存数量
        }


        /// <summary>
        /// 修改出库信息
        /// </summary>
        /// <param name="outStore"></param>
        /// <returns></returns>
        public int ReviseOutStoreInfo(OutStore outStore)
        {
            return outStoreService.ReviseOutStoreInfo(outStore);
        }


        /// <summary>
        /// 删除出库信息
        /// </summary>
        /// <param name="outStore"></param>
        /// <returns></returns>
        public int DeleteOutStoreInfo(OutStore outStore)
        {
            return outStoreService.DeleteOutStoreInfo(outStore);
        }
    }
}
