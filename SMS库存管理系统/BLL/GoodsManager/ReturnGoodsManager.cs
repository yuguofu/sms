﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;

namespace BLL.GoodsManager
{
    public class ReturnGoodsManager
    {
        private DAL.GoodsManager.ReturnGoodsService returnGoodsService = new DAL.GoodsManager.ReturnGoodsService();

        /// <summary>
        /// 获取 还货表dataset
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSetFromReturnGoodsTable()
        {
            return returnGoodsService.GetDataSetFromReturnGoodsTable();
        }


        /// <summary>
        /// 还货
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int ReturnGoods(ReturnGoods returnGoods)
        {
            return returnGoodsService.ReturnGoods(returnGoods);

        }


        /// <summary>
        /// 还货时更新库存
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int UpdateStorageOnReturnGoods(ReturnGoods returnGoods)
        {
            return returnGoodsService.UpdateStorageOnReturnGoods(returnGoods);
        }

        /// <summary>
        /// 修改还货信息
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int ReviseReturnGoodsInfo(ReturnGoods returnGoods)
        {
            return returnGoodsService.ReviseReturnGoodsInfo(returnGoods);
        }


        /// <summary>
        /// 删除还货信息
        /// </summary>
        /// <param name="returnGoods"></param>
        /// <returns></returns>
        public int DeleteReturnGoodsInfo(ReturnGoods returnGoods)
        {
            return returnGoodsService.DeleteReturnGoodsInfo(returnGoods);
        }
    }
}
