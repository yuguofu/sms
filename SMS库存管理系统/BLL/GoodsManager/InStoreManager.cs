﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SMSModels;


namespace BLL.GoodsManager
{
    public class InStoreManager
    {
        private DAL.GoodsManager.InStoreService InStoreService = new DAL.GoodsManager.InStoreService();

        /// <summary>
        /// 获取入库表数据集对象
        /// </summary>
        /// <returns></returns>
        public DataSet GetDataSetFromInStoreTable()
        {
            return InStoreService.GetDataSetFromInStoreTable();

        }


        /// <summary>
        /// 货物入库
        /// </summary>
        /// <param name="inStore"></param>
        /// <returns></returns>
        public int InStore(InStore inStore)
        {
            return InStoreService.InStore(inStore);
        }


        /// <summary>
        /// 库存表中是否存在该记录
        /// </summary>
        /// <param name="inStore"></param>
        /// <returns>存在返回true，不存在返回false</returns>
        public bool IsExists(InStore inStore)
        {
            return InStoreService.IsExists(inStore);
        }


        /// <summary>
        /// 入库更时新库存数量
        /// </summary>
        /// <param name="inStore"></param>
        /// <returns></returns>
        public int UpdateStorageOnInStore(InStore inStore)
        {
            return InStoreService.UpdateStorageOnInStore(inStore); //更新库存数量
        }


        /// <summary>
        /// 插入库存表数据
        /// </summary>
        /// <param name="inStore"></param>
        /// <returns></returns>
        public int InsertStorageTable(InStore inStore)
        {

            return InStoreService.InsertStorageTable(inStore); //插入库存表数据

        }


        /// <summary>
        /// 修改入库信息
        /// </summary>
        /// <param name="inStore"></param>
        /// <returns></returns>
        public int ReviseInStoreInfo(InStore inStore)
        {
            return InStoreService.ReviseInStoreInfo(inStore);
        }


        /// <summary>
        /// 删除入库信息
        /// </summary>
        /// <param name="inStore"></param>
        /// <returns></returns>
        public int DeleteInStoreInfo(InStore inStore)
        {
            return InStoreService.DeleteInStoreInfo(inStore);
        }
    }
}
