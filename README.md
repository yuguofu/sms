# SMS库存管理系统

#### 介绍
winform SMS库存管理系统，初学者作品，仅供学习交流，请勿在生成环境使用。


#### 软件架构
.net framework4.6.2, sqlite3数据库


#### 安装教程
visual studio 2019打开编辑、编译

#### 使用说明
visual studio 2019打开编辑、编译  

 _sqlite3数据库文件已补上，文件名 > sms_db_test.db ，运行程序请把数据库文件放到` SMSUI\bin\Debug\` （或Release）目录下，可用sqlitestudio打开编辑_   

用户名 admin 密码 123456

 _另：可在调试编译文件路径（SMSUI\bin\Debug\）双击执行  **库存管理系统.exe** 查看运行效果_   


#### 预览
![登录](https://images.gitee.com/uploads/images/2020/1119/205125_024fe7ff_5348320.png "屏幕截图.png")
![主界面](https://images.gitee.com/uploads/images/2020/1119/205138_e4898b5e_5348320.png "屏幕截图.png")
![操作界面](https://images.gitee.com/uploads/images/2020/1119/205232_aa4291ab_5348320.png "屏幕截图.png")
